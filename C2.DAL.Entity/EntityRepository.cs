﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DAL.Entity
{
    /// <summary>
    /// Репозиторий для работы с сущностями одного типа.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public class EntityRepository<TEntity>
        where TEntity : class
    {
        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="context"></param>
        public EntityRepository(DbContext context)
        {
            Context = context;
            DbSet = Context.Set<TEntity>();
        }

        public DbContext Context
        {
            get;
            private set;
        }

        public DbSet<TEntity> DbSet
        {
            get;
            private set;
        }

        /// <summary>
        /// Получение сущности по ключам.
        /// </summary>
        /// <param name="keyValues"></param>
        /// <returns></returns>
        public virtual TEntity GetById(params object[] keyValues)
        {
            return DbSet.Find(keyValues);
        }

        /// <summary>
        /// Выбор сущностей.
        /// </summary>
        /// <param name="@params"></param>
        /// <returns></returns>
        public SelectResult<TEntity> Select(SelectParams<TEntity> @params)
        {
            IQueryable<TEntity> baseQuery = DbSet.Apply(@params.Filters);

            var data = baseQuery
                .Apply(@params.Includes)
                .Apply(@params.Orders)
                .Skip(@params.PageIndex * @params.PageSize)
                .Take(@params.PageSize)
                .ToList();

            var totalCount = baseQuery
                .LongCount();

            return new SelectResult<TEntity>(data, totalCount, @params.PageIndex, @params.PageSize);
        }

        /// <summary>
        /// Добавление сущности.
        /// </summary>
        /// <param name="entity"></param>
        public virtual void Add(TEntity entity)
        {
            DbSet.Add(entity);
        }

        /// <summary>
        /// Обновление сущности.
        /// </summary>
        /// <param name="entity"></param>
        public virtual void Update(TEntity entity)
        {
            var entry = Context.Entry(entity);
            if (entry.State == EntityState.Detached)
            {
                DbSet.Attach(entity);
            }
            entry.State = EntityState.Modified;
        }

        /// <summary>
        /// Удаление сущности.
        /// </summary>
        /// <param name="entity"></param>
        /// <remarks>По умолчанию эта операция не реализована, т.к. сущности должны не удаляться, а только помечаться как удалённые.</remarks>
        public virtual void Delete(TEntity entity)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Поиск сущности и её удаление.
        /// </summary>
        /// <param name="keyValues"></param>
        public virtual void Delete(params object[] keyValues)
        {
            TEntity entity = GetById(keyValues);
            Delete(entity);
        }
    }
}
