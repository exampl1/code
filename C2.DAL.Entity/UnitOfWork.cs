﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DAL.Entity
{
    public abstract class UnitOfWork<TContext> : IDisposable
        where TContext : DbContext
    {
        protected UnitOfWork(TContext context)
        {
            Context = context;
        }

        ~UnitOfWork()
        {
            Dispose(false);
        }

        protected readonly TContext Context;

        protected EntityRepository<TEntity> CreateRepository<TEntity>()
            where TEntity : class
        {
            return new EntityRepository<TEntity>(Context);
        }

        public void Save()
        {
            Context.SaveChanges();
        }

        private bool _disposed = false;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    Context.Dispose();
                }
            }
            _disposed = true;
        }
    }
}
