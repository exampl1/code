﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace C2.DAL.Entity
{
    public static class QueryableHelper
    {
        /// <summary>
        /// Сортировка.
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="query"></param>
        /// <param name="keySelector"></param>
        /// <param name="isAsc"></param>
        /// <param name="first"></param>
        /// <returns></returns>
        public static IQueryable<TEntity> Order<TEntity, TProperty>(this IQueryable<TEntity> query,
            Expression<Func<TEntity, TProperty>> keySelector,
            bool isAsc = true,
            bool first = true)
        {
            if (!first)
            {
                // Query already ordered.
                IOrderedQueryable<TEntity> orderedQuery = (IOrderedQueryable<TEntity>)query;
                return isAsc ? orderedQuery.ThenBy(keySelector) : orderedQuery.ThenByDescending(keySelector);
            }
            else
            {
                return isAsc ? query.OrderBy(keySelector) : query.OrderByDescending(keySelector);
            }
        }
    }
}
