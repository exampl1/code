﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.Globalization
{
    /// <summary>
    /// Получение локализованных ресурсов из базы данных MS SQL.
    /// </summary>
    public class SqlResourceProvider : IResourceProvider
    {
        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="connectionString">Строка подключения к БД.</param>
        /// <param name="selectCommandPrototype">Прототип команды для извлчения локализованных ресурсов из базы данных.</param>
        /// <remarks>
        /// В любом случае необходимо, что команда принимала в качестве первого параметра код культуры, а в качестве результата
        /// возвращала набор данных, содержащих два текстовых поля: первое - ключ строки, второе - саму строку.
        /// </remarks>
        public SqlResourceProvider(string connectionString, SqlCommand selectCommandPrototype = null)
        {
            _connectionString = connectionString;
            _selectCommandPrototype = selectCommandPrototype ?? CreateDefaultPrototype();
        }

        string _connectionString;

        SqlCommand _selectCommandPrototype;

        SqlCommand CreateDefaultPrototype()
        {
            var command = new SqlCommand("select [Key], [Value] from [dbo].[Locales] where Culture = @culture");
            command.Parameters.Add(new SqlParameter("@culture", string.Empty));
            return command;
        }

        /// <summary>
        /// Не поддерживается.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="culture"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool TryGetString(string key, string culture, out string value)
        {
            throw new NotSupportedException();
        }

        public bool TryGetStrings(string culture, out Dictionary<string, string> values)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (SqlCommand command = _selectCommandPrototype.Clone())
                {
                    command.Connection = connection;
                    command.Parameters[0].Value = culture;

                    values = new Dictionary<string, string>();
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            values.Add(reader.GetString(0), reader.GetString(1));
                        }
                    }
                }
            }

            return (values.Count > 0);
        }
    }
}
