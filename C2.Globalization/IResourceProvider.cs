﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace C2.Globalization
{
    public interface IResourceProvider
    {
        bool TryGetString(string key,
            string culture,
            out string value);

        bool TryGetStrings(string culture,
            out Dictionary<string, string> values);
    }
}
