﻿using C2.AppModel;
using C2.DAL;
using C2.DataManager.Store;
using C2.DataModel;
using C2.DataModel.StoreModel;
using C2.WebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace C2.WebApp.Store.Models
{
    /// <summary>
    /// модель данных для страницы товара
    /// </summary>
    public sealed class ProductItemViewModel
    {
        /// <summary>
        /// Отображаемый продукт
        /// </summary>
        public Product Product { get; set; }

        /// <summary>
        /// Случайный коммент
        /// </summary>
        public Comment RandomComment { get; set; }

        /// <summary>
        /// Следующий продукт
        /// </summary>
        public Product Next { get; set; }


        /// <summary>
        /// Предыдущий продукт
        /// </summary>
        public Product Previous { get; set; }
    }

    /// <summary>
    /// Модель данных для "товарных" страниц.
    /// </summary>
    public sealed class ProductPageModel
    {

        public SelectProductParams SelectParams
        {
            get;
            set;
        }

        public CustomerInfo CustomerInfo
        {
            get;
            set;
        }

        /// <summary>
        /// Результаты поиска
        /// </summary>
        public SelectResult<Product> SelectResult
        {
            get;
            set;
        }

        /// <summary>
        /// Товар доп категории
        /// </summary>
        public SelectResult<Product> Select1Result
        {
            get;
            set;
        }


        /// <summary>
        /// Контент
        /// </summary>
        public PageContent PageContent
        {
            get;
            set;
        }
        
        /// <summary>
        /// Доп контент
        /// </summary>
        public PageContent AddPageContent
        {
            get;
            set;
        }
        /// <summary>
        /// Каталог товаров.
        /// </summary>
        public TreeViewModel Catalog
        {
            get;
            set;
        }

        /// <summary>
        /// Идентфикатор выбранного узла каталога.
        /// </summary>
        public long CategoryNodeId
        {
            get;
            set;
        }

        /// <summary>
        /// Выбранная товарная группа.
        /// </summary>
        public ProductGroup SelectedProductGroup
        {
            get
            {
                TreeNodeModel node;
                if (Catalog.TryFind(CategoryNodeId, out node))
                {
                    return (ProductGroup)node.Value;
                }
                else
                {
                    return null;
                }
            }
        }

        //List<ClassifierNodeAppModel<ProductGroup>> _expandedNodes;

        ///// <summary>
        ///// Отпределяет, должен ли узел быть раскрытым.
        ///// </summary>
        ///// <param name="node"></param>
        ///// <returns></returns>
        //public bool IsExpandedNode(ClassifierNodeAppModel<ProductGroup> node)
        //{
        //    return (_expandedNodes != null) && _expandedNodes.Contains(node);
        //}

        ///// <summary>
        ///// Проверка, что узел является активным.
        ///// </summary>
        ///// <param name="node"></param>
        ///// <returns></returns>
        //public bool IsActiveNode(ClassifierNodeAppModel<ProductGroup> node)
        //{
        //    return node == _activeNode;
        //}

        /// <summary>
        /// Информация о покупательской корзине.
        /// </summary>
        public Order Basket
        {
            get;
            set;
        }
    }

    ///// <summary>
    ///// Модель данных для индексной страницы товаров.
    ///// </summary>
    //public class ProductIndexPageModel : ProductPageModel
    //{
    //    /// <summary>
    //    /// Результат поиска товаров.
    //    /// </summary>
    //    public SelectResult<Product> Products
    //    {
    //        get;
    //        set;
    //    }
    //}

    ///// <summary>
    ///// Модель данных для страницы просмотра товара.
    ///// </summary>
    //public class ProductItemModel : ProductPageModel
    //{
    //    public Product Product
    //    {
    //        get;
    //        set;
    //    }
    //}
}