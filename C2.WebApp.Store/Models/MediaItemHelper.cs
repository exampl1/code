﻿using C2.DataModel.MediaModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace C2.WebApp.Store.Models
{
    public static class MediaItemHelper
    {
        public static string AsPreview(this MediaCollectionEntry media)
        {
            return string.Format(media.MediaItem.PermaLink, "prv");
        }

        public static string AsFull(this MediaCollectionEntry media)
        {
            return string.Format(media.MediaItem.PermaLink, "full");
        }

        public static string AsOriginal(this MediaCollectionEntry media)
        {
            return string.Format(media.MediaItem.PermaLink, "orig");
        }
    }
}