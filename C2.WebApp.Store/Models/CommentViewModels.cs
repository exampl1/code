﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using C2.DataModel;

namespace C2.WebApp.Store.Models
{
    public class CommentViewModel
    {
        public IEnumerable<Comment> CommentList { get; set; }
    }
}