﻿using C2.DataModel;
using C2.DataModel.StoreModel;
using C2.WebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace C2.WebApp.Store.Areas.Admin.Models
{
    public class CustomerViewModel
    {
        public UserProfile UserProfile
        {
            get;
            set;
        }

        public CustomerInfo CustomerInfo
        {
            get;
            set;
        }

        public CustomerProfile CustomerProfile
        {
            get;
            set;
        }


    }

    public class UserViewModel
    {
        public UserViewParameters Parameters
        {
            get;
            set;
        }

        public IList<CustomerViewModel> CustomerUserInfos
        {
            get;
            set;
        }
    }

    public class UserViewParameters 
    {
    }

}