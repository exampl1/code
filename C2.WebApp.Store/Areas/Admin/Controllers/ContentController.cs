﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using C2.WebApp.Models;
using C2.WebApp.Store.Models;
using C2.DataMapping;
using C2.DataManager;
using C2.AppController;
using C2.DataModel;

namespace C2.WebApp.Store.Areas.Admin.Controllers
{
    [Authorize(Roles="Admin")]
    public class ContentController : Controller
    {

        public ContentController()
        {
            _dataContext = new AppContext("DefaultConnection");
            _dataManager = new AppDataManager(_dataContext);
        }

        AppContext _dataContext;
        AppDataManager _dataManager;

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _dataContext.Dispose();
            }

            base.Dispose(disposing);
        }

        //
        // GET: /Admin/Content/
        public ActionResult Index()
        {
            ViewBag.Supplier = SupplierModel.Current.Subject.CurrentLocalization.FullName;
            return View(new ContentPageModel
            {
                AllPagesContent = _dataManager.GetContentPages().Where(c => c.ContentType != "text/html-email").ToList()
            });
        }

        // GET: /Admin/Content/
        public ActionResult CreateEdit(long? id)
        {
            PageContent page = null;

            if (id.HasValue)
            {
                page = _dataManager.GetContentPages().FirstOrDefault(p => p.Id == id.Value);
            }
            else
            {
                page = new PageContent { ContentType = "text/html" };
            }

            return View(page);
        }

        // Post: /Admin/Content/
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateEdit(PageContent model)
        {
            try
            {
                _dataManager.SaveContentPage(model);
                return RedirectToAction("Index", new { controller = model.ContentType == "text/html-email" ? "Spam" : Request.RequestContext.RouteData.Values["controller"] });
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex);
            }

            return View(model);
        }
    }
}
