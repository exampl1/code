﻿using C2.DataManager.Store;
using C2.DataMapping.Store;
using C2.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace C2.WebApp.Store.Controllers
{
    public class CommentController : Controller
    {
        public CommentController()
        {
            _dataContext = new StoreContext("DefaultConnection");
            _dataManager = new StoreDataManager(_dataContext);
        }

        StoreContext _dataContext;
        StoreDataManager _dataManager;

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _dataContext.Dispose();
            }

            base.Dispose(disposing);
        }

        // GET: /Home/feedback
        public ActionResult Index(int? id, int? page)
        {
            if (id.HasValue)
            {
                //отдельный камент
                var comment = _dataManager.GetComment(id.Value);
                if (comment != null)
                {
                    return View("Comment", comment);
                }
            }
            else
            {
                //значит хотим список
                if (User.Identity.IsAuthenticated)
                {
                    //получим имя для авторизованного
                    var userContext = new C2.DataMapping.UsersContext();
                    var userProfile = userContext.UserProfiles.Find(WebMatrix.WebData.WebSecurity.CurrentUserId);
                    ViewBag.UserDisplayName = userProfile.FirstName ?? userProfile.UserName;
                }
                
                var comments = _dataManager.GetComments(page);
                if (comments != null)
                {
                    return View(comments);
                }

            }

            throw new HttpException(404, string.Format("Страница {0} не найдена", id));
        }

        [HttpPost]
        [Authorize]
        public ActionResult AddComment(Comment model)
        {
            if (ModelState.IsValid)
            {
                model.UserName = User.Identity.Name;
                model.Status = EntityStatus.Normal;
                model.Path = "";
                model.Created = DateTime.UtcNow;

                _dataManager.AddComment(model);

                return new ContentResult
                {
                    Content = @"Комментарий успешно добавлен
                    <script>
                    setTimeout(function() {
                        location.href = location.href;
                    }, 3000);
                    </script>",
                };
            }

            return new ContentResult
            {
                Content = "Невозможно добавить комментарий",
            };

        }


    }
}
