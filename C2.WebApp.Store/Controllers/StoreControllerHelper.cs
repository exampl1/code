﻿using C2.WebApp.Store.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace C2.WebApp.Store
{
    public static class StoreControllerHelper
    {
        public static ProductPageModel GetPageModel(this ViewDataDictionary data)
        {
            return (data["model"] as ProductPageModel);
        }

        public static void SetPageModel(this ViewDataDictionary data, ProductPageModel model)
        {
            data["model"] = model;
        }
    }
}