﻿using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using C2.DataMapping.Store;
using Newtonsoft.Json;
using C2.WebApp.Store.Models;
using C2.DataManager.Store;
using System.Web.Profile;
using System.Web;
using System;
using C2.DataModel.StoreModel;
using C2.DataMapping;
using C2.DataModel;
using WebMatrix.WebData;

namespace C2.WebApp.Store.Controllers
{
    //[Authorize]
    public class BasketController : Controller
    {
        public BasketController()
        {
            _dataContext = new StoreContext("DefaultConnection");
            _dataManager = new StoreDataManager(_dataContext);
        }

        StoreContext _dataContext;

        StoreDataManager _dataManager;

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _dataContext.Dispose();
            }

            base.Dispose(disposing);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Get()
        {
            return PartialView("BasketInfo", BasketController.GetOrder(_dataContext, Session));
        }

        //
        // GET: /Basket/

        /// <summary>
        /// Возвращает список товарных позиций в корзине пользователя.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            var order = GetOrder(_dataContext, Session);

            var model = new ProductPageModel { Basket = order };
            model.Catalog = new WebApp.Models.TreeViewModel("_ProductGroupHeader",
                _dataManager.GetProductGroupClassifier().Roots,
                null,
                null);

            return View(model);
        }

        //
        // POST: /Basket/Add/{id}
        [HttpPost]
        public ActionResult Add(long[] id, int[] count, int isPreorder = 0)
        {
            var customerInfo = ViewData.GetPageModel().CustomerInfo;
            bool isWholesale = (customerInfo != null && customerInfo.Status == CustomerStatus.IsWholesale);

            if (id.Length != count.Length)
            {
                throw new ArgumentException("id.Length != count.Length");
            }

            if (count.Any(c => c < 0))
            {
                throw new ArgumentException("Количество не может быть отрицательным.");
            }

            // Найдём элемент в корзине или создадим новый.
            var order = GetOrder(_dataContext, Session);
            if (order.Id == 0)
            {
                order.Id = BaseContext.GetId();
                _dataContext.Orders.Add(order);
                _dataContext.SaveChanges();
                order = _dataContext.LoadOrderDetails(order.Id);
            };

            order.IsPreorder = isPreorder > 0;

            for (int i = 0; i < id.Length; i++)
            {
                long _id = id[i];
                var offer = _dataContext.LoadOfferDetails(_id);

                if (offer == null)
                {
                    throw new HttpException(404, string.Format("Offer {0} не найден", _id));
                    // Ошибка - невозможно подобрать цену для товара
                    // TODO: сообщить, что цену узнать можно по запросу способами а), б) и в)
                }

                var orderItem = order.Items.FirstOrDefault(oi => (oi.OfferId == offer.Id));
                if (orderItem == null)
                {
                    orderItem = new OrderItem()
                    {
                        Id = BaseContext.GetId(),
                        Order = order,
                        Offer = offer,
                        PriceValue = isWholesale ? offer.PriceOpt40.GetValueOrDefault(offer.Price) : offer.PriceOpt20.HasValue ? offer.Price * (1 - offer.PriceOpt20.Value / 100) : offer.Price,
                        Quantity = 0
                    };
                    order.Items.Add(orderItem);
                }

                orderItem.Quantity += count[i];

                if (orderItem.Quantity > offer.AvailQuantity && isPreorder == 0)
                {
                    // Нельзя превышать запасы, если это не предзаказ
                    orderItem.Quantity = offer.AvailQuantity;
                }
            }

            order.UpdateTotals(isWholesale: isWholesale);

            // Сохраним
            _dataContext.SaveChanges();

            if (!WebSecurity.IsAuthenticated)
            {
                Session["basket"] = order.Id.ToString();
            }

            if (Request.IsAjaxRequest())
            {
                return PartialView("BasketInfo", order);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public ActionResult SetQuantity(long id, int quantity, int isPreorder = 0)
        {
            if (quantity < 0)
            {
                throw new ArgumentException("Количество не может быть отрицательным.");
            }

            var customerInfo = ViewData.GetPageModel().CustomerInfo;
            bool isWholesale = (customerInfo != null && customerInfo.Status == CustomerStatus.IsWholesale);

            // Найдём элемент в корзине.
            var order = GetOrder(_dataContext, Session);
            if (order.Id == 0)
            {
                order.Id = BaseContext.GetId();
                _dataContext.Orders.Add(order);
            };
            
            order.IsPreorder = isPreorder > 0;

            var orderItem = order.Items.FirstOrDefault(oi => (oi.OfferId == id));
            if (orderItem != null)
            {
                if (quantity == 0)
                {
                    order.Items.Remove(orderItem);
                    _dataContext.OrderItems.Remove(orderItem);
                }
                else
                {

                    var offer = _dataContext.LoadOfferDetails(id);

                    if (offer == null)
                    {
                        // Ошибка - невозможно подобрать цену для товара
                        // TODO: сообщить, что цену узнать можно по запросу способами а), б) и в)

                    }

                    orderItem.Quantity = quantity;
                    if (orderItem.Quantity > offer.AvailQuantity && isPreorder == 0)
                    {
                        // Нельзя превышать запасы.
                        orderItem.Quantity = offer.AvailQuantity;
                    }
                }

                order.UpdateTotals(isWholesale: isWholesale);
                
                _dataContext.SaveChanges();
            }

            if (Request.IsAjaxRequest())
            {
                return PartialView("BasketItems", order);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }


        // POST: /Basket/Remove/{id}

        /// <summary>
        /// Удаление товарной позиции из корзины.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Remove(long id)
        {
            var customerInfo = ViewData.GetPageModel().CustomerInfo;
            bool isWholesale = (customerInfo != null && customerInfo.Status == CustomerStatus.IsWholesale);

            var order = GetOrder(_dataContext, Session);
            if (order.Id == 0)
            {
                order.Id = BaseContext.GetId();
                _dataContext.Orders.Add(order);
            };
            
            var orderItem = order.Items.FirstOrDefault(oi => (oi.OfferId == id));
            if (orderItem != null)
            {
                _dataContext.OrderItems.Remove(orderItem);
            }

            order.UpdateTotals(isWholesale: isWholesale);

            _dataContext.SaveChanges();

            if (Request.IsAjaxRequest())
            {
                return PartialView("BasketItems", order);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        internal static Order GetOrder(StoreContext dataContext, HttpSessionStateBase session)
        {
            long orderId;
            Order order;

            // Для пользователя пытаемся найти заказ в состоянии "Оформляется заказчиком" в базе.
            if (WebSecurity.IsAuthenticated)
            {
                order = dataContext.Orders.FirstOrDefault(o => (o.CustomerId == WebSecurity.CurrentUserName) && (o.OrderStatus == OrderStatus.Creation | o.OrderStatus == OrderStatus.Preorder));
                if (order != null)
                {
                    return dataContext.LoadOrderDetails(order.Id);
                }
            }

            if ((session != null) &&
                (long.TryParse(session["basket"] as string, out orderId)) &&
                (orderId > 0) &&
                ((order = dataContext.LoadOrderDetails(orderId)) != null))
            {
                if ((order.CustomerId == "-") && WebSecurity.IsAuthenticated)
                {
                    // Привяжем анонимный заказ к пользователю.
                    order.CustomerId = WebSecurity.CurrentUserName;
                    order.PayerId = order.CustomerId;
                    
                    dataContext.SaveChanges();

                    session.Remove("basket");
                }

                return order;
            }

            order = new Order()
            {
                CreateDate = DateTime.UtcNow,
                Status = EntityStatus.Normal,
                OrderStatus = OrderStatus.Creation,
                CurrencyCode = "RUB",
            };

            order.CustomerId = (WebSecurity.IsAuthenticated ? WebSecurity.CurrentUserName : "-");
            order.PayerId = order.CustomerId;

            return order;
        }

        //internal static BasketModel GetOrder(HttpSessionStateBase session)
        //{
        //    BasketModel order = null;
        //    if (session != null && session["basket"] as string != null)
        //    {
        //        order = JsonConvert.DeserializeObject<BasketModel>(session["basket"] as string, new JsonSerializerSettings()
        //        {
        //            NullValueHandling = NullValueHandling.Ignore,
        //            PreserveReferencesHandling = PreserveReferencesHandling.All,
        //            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
        //        });
        //    }
            
        //    if (order == null)
        //    {
        //        order = new BasketModel();
        //    }

        //    return order;
        //}

        //internal static void SetOrder(HttpSessionStateBase session, BasketModel basket)
        //{
        //    if (basket != null)
        //    {
        //        session["basket"] = JsonConvert.SerializeObject(basket, new JsonSerializerSettings()
        //        {
        //            NullValueHandling = NullValueHandling.Ignore,
        //            PreserveReferencesHandling = PreserveReferencesHandling.All,
        //            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
        //        });
        //    }
        //    else
        //    {
        //        session["basket"] = null;
        //    }

        //}

        //void RestoreOrderItemsData(BasketModel order)
        //{
        //    foreach (var item in order.Items)
        //    {
        //        // Необходимо восстановить не сохраняемую информацию о заказе.
        //        item.Offer = _dataContext.Offers
        //            .Include(o => o.Product)
        //            .Include(o => o.Product.Localization)
        //            .Include(o => o.Properties)
        //            .Include(o => o.Properties.Select(pr => pr.PropertyType))
        //            .Include(o => o.Properties.Select(pr => pr.PropertyType.Localization))
        //            .Include(o => o.Unit)
        //            .Include(o => o.Unit.Localization)
        //            .Include(o => o.Product.MediaCollection)
        //            .Include(o => o.Product.MediaCollection.Select(m => m.MediaItem))
        //            .Include(o => o.Product.MediaCollection.Select(m => m.MediaItem.Localization))
        //            .FirstOrDefault(i => i.Id == item.OfferId);
        //    }
            
        //    // Удалим все несуществующие элементы.
        //    order.Items.RemoveAll(i => i.Offer == null);
        //}
    }
}
