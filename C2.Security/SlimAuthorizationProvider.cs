﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using C2.Logging;

namespace C2.Security
{
    public class SlimAuthorizationProvider : IAuthorizationProvider
    {
        public SlimAuthorizationProvider(BaseLogger logger)
        {
            _logger = logger;
        }

        public bool IsAuthorized(IPrincipal principal, string ruleName)
        {
            AuthorizationRule rule;
            if (!_rules.TryGetValue(ruleName, out rule))
            {
                _logger.Error(Localization.AuthorizationRuleNotFound, ruleName);
                return false;
            }

            return rule.IsAuthorized(principal);
        }

        /// <summary>
        /// Add multiple rules to provider.
        /// </summary>
        /// <param name="rulesConfig">Rules config in format "rule1; rule2; rule3".</param>
        public void AddRules(string rulesConfig)
        {
            if (string.IsNullOrWhiteSpace(rulesConfig))
            {
                _logger.Error(Localization.InvalidFormatRuleString, rulesConfig);
                return;
            }

            foreach (string ruleConfig in rulesConfig.Split(';'))
            {
                AddRule(ruleConfig);
            }
        }

        /// <summary>
        /// Add rule to provider.
        /// </summary>
        /// <param name="ruleConfig">Rule's config in format "name=role1, !role2".</rule_name></param>
        public void AddRule(string ruleConfig)
        {
            // Parse rule.
            if (string.IsNullOrWhiteSpace(ruleConfig))
            {
                _logger.Error(Localization.InvalidFormatRuleString, ruleConfig);
                return;
            }

            string[] segments = ruleConfig.Trim().Split('=');
            if (segments.Length != 2)
            {
                _logger.Error(Localization.InvalidFormatRuleString, ruleConfig);
                return;
            }

            string ruleName = segments[0];
            string rawRoles = segments[1];

            if (string.IsNullOrWhiteSpace(rawRoles))
            {
                _logger.Error(Localization.InvalidFormatRuleString, ruleConfig);
                return;
            }

            string[] roles = rawRoles.Split(',');
            List<string> allowedRoles = new List<string>();
            List<string> deniedRoles = new List<string>();

            foreach (string role in roles)
            {
                if (!string.IsNullOrWhiteSpace(role))
                {
                    string clearValue = role.Trim();
                    if (clearValue[0] == '!')
                    {
                        deniedRoles.Add(clearValue.TrimStart('!'));
                    }
                    else
                    {
                        allowedRoles.Add(clearValue);
                    }
                }
            }

            if ((allowedRoles.Count == 0) && (deniedRoles.Count == 0))
            {
                _logger.Error(Localization.InvalidFormatRuleString, ruleConfig);
                return;
            }
            
            // Add parsed rule.
            AddRule(ruleName,
                allowedRoles.ToArray(),
                deniedRoles.ToArray());
        }

        public void AddRule(string ruleName, string[] allowedRoles, string[] deniedRoles)
        {
            _rules.Add(ruleName, new AuthorizationRule()
            {
                AllowedRoles = allowedRoles,
                DeniedRoles = deniedRoles
            });
        }

        BaseLogger _logger;

        Dictionary<string, AuthorizationRule> _rules = new Dictionary<string, AuthorizationRule>();

        class AuthorizationRule
        {
            public bool IsAuthorized(IPrincipal principal)
            {
                // Check denied roles.
                for (int i = 0; i < DeniedRoles.Length; i++)
                {
                    if (principal.IsInRole(DeniedRoles[i]))
                    {
                        return false;
                    }
                }

                // Check enabled roles.
                for (int i = 0; i < AllowedRoles.Length; i++)
                {
                    if (principal.IsInRole(AllowedRoles[i]))
                    {
                        return true;
                    }
                }

                // By default access is denied.
                return false;
            }

            public string[] AllowedRoles
            {
                get;
                set;
            }

            public string[] DeniedRoles
            {
                get;
                set;
            }
        }
    }
}
