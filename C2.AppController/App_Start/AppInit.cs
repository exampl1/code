﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.AppController
{

    public interface IAppInitProvider
    {
        IEnumerable<AppIniter> GetIniters();
    }

    public interface IIniter
    {
        void Init();

        int Order
        {
            get;
        }
    }

    public class AppIniter
    {
        public const int DefaultOrder = -1;
        public object Instance
        {
            get;
            protected set;
        }
        public int Order
        {
            get;
            protected set;
        }
        public AppIniter(object instance, int? order)
        {
            if (instance == null)
            {
                throw new ArgumentNullException("instance");
            }
            if (!order.HasValue)
            {
                IIniter initer = instance as IIniter;
                if (initer != null)
                {
                    order = new int?(initer.Order);
                }
            }
            this.Instance = instance;
            int? num = order;
            this.Order = (num.HasValue ? num.GetValueOrDefault() : -1);
        }
    }

    public static class GlobalIniters
    {
        public static AppInitCollection Initers
        {
            get;
            private set;
        }

        public static void Init()
        {
            Initers
                .OrderBy(i => i.Order)
                .ToList()
                .ForEach(i =>
            {
                var initer = i.Instance as IIniter;
                if (initer != null)
                {
                    initer.Init();
                }
            });
        }

        static GlobalIniters()
        {
            GlobalIniters.Initers = new AppInitCollection();
        }
    }

    public sealed class AppInitCollection : IEnumerable<AppIniter>, IEnumerable, IAppInitProvider
    {
        private List<AppIniter> _initers = new List<AppIniter>();
        public int Count
        {
            get
            {
                return this._initers.Count;
            }
        }
        public void Add(object initer)
        {
            this.AddInternal(initer, null);
        }
        public void Add(object initer, int order)
        {
            this.AddInternal(initer, new int?(order));
        }
        private void AddInternal(object initer, int? order)
        {
            AppInitCollection.ValidateiniterInstance(initer);
            this._initers.Add(new AppIniter(initer, order));
        }
        public void Clear()
        {
            this._initers.Clear();
        }
        public bool Contains(object initer)
        {
            return this._initers.Any((AppIniter f) => f.Instance == initer);
        }
        public IEnumerator<AppIniter> GetEnumerator()
        {
            return this._initers.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this._initers.GetEnumerator();
        }
        IEnumerable<AppIniter> IAppInitProvider.GetIniters()
        {
            return this;
        }
        public void Remove(object initer)
        {
            this._initers.RemoveAll((AppIniter f) => f.Instance == initer);
        }
        private static void ValidateiniterInstance(object instance)
        {
            if (instance != null && !(instance is IIniter))
            {
                throw new InvalidOperationException();
            }
        }
    }
}
