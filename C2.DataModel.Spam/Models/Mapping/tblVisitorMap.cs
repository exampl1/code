using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace C2.DataModel.Spam.Models.Mapping
{
    public class tblVisitorMap : EntityTypeConfiguration<tblVisitor>
    {
        public tblVisitorMap()
        {
            // Primary Key
            this.HasKey(t => t.VisitorId);

            // Properties
            this.Property(t => t.Email)
                .HasMaxLength(128);

            this.Property(t => t.FirstName)
                .HasMaxLength(64);

            this.Property(t => t.LastName)
                .HasMaxLength(64);

            this.Property(t => t.Title)
                .HasMaxLength(1024);

            this.Property(t => t.Phone)
                .HasMaxLength(32);

            // Table & Column Mappings
            this.ToTable("tblVisitor");
            this.Property(t => t.VisitorId).HasColumnName("VisitorId");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.CityId).HasColumnName("CityId");
            this.Property(t => t.FirstName).HasColumnName("FirstName");
            this.Property(t => t.LastName).HasColumnName("LastName");
            this.Property(t => t.Gender).HasColumnName("Gender");
            this.Property(t => t.Title).HasColumnName("Title");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.CreateDate).HasColumnName("CreateDate");
            this.Property(t => t.Status).HasColumnName("Status");
        }
    }
}
