using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace C2.DataModel.Spam.Models.Mapping
{
    public class tblSessionMap : EntityTypeConfiguration<tblSession>
    {
        public tblSessionMap()
        {
            // Primary Key
            this.HasKey(t => t.Session);

            // Properties
            this.Property(t => t.Session)
                .IsRequired()
                .HasMaxLength(256);

            this.Property(t => t.Data)
                .HasMaxLength(256);

            this.Property(t => t.IP)
                .HasMaxLength(64);

            this.Property(t => t.Version)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("tblSession");
            this.Property(t => t.Session).HasColumnName("Session");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.ActivatorId).HasColumnName("ActivatorId");
            this.Property(t => t.CreateDate).HasColumnName("CreateDate");
            this.Property(t => t.ExpiredDate).HasColumnName("ExpiredDate");
            this.Property(t => t.CloseDate).HasColumnName("CloseDate");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.Data).HasColumnName("Data");
            this.Property(t => t.DataId).HasColumnName("DataId");
            this.Property(t => t.LastActiveDate).HasColumnName("LastActiveDate");
            this.Property(t => t.IP).HasColumnName("IP");
            this.Property(t => t.Version).HasColumnName("Version");

        }
    }
}
