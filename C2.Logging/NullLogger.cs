﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.Logging
{
    /// <summary>
    /// Логгер-заглушка.
    /// </summary>
    public class NullLogger : BaseLogger
    {
        public override void Log(LogMessageSeverity severity,
            string messageFormat,
            params object[] formatParams)
        {
            return;
        }
    }
}
