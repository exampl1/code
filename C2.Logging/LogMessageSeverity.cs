﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.Logging
{
    /// <summary>
    /// Важность логируемых сообщений.
    /// </summary>
    public enum LogMessageSeverity
    {
        /// <summary>
        /// Отладочная информация.
        /// </summary>
        Debug,

        /// <summary>
        /// Трассировка.
        /// </summary>
        Trace,

        /// <summary>
        /// Информационное сообщение.
        /// </summary>
        Information,

        /// <summary>
        /// Предупреждение.
        /// </summary>
        /// <remarks>
        /// Не ошибка, но что-то странное, требуется дополнительный анализ ситуации.
        /// </remarks>
        Warning,

        /// <summary>
        /// Ошибка, после которого система продолжает функционировать в обычном режиме.
        /// </summary>
        /// <remarks>
        /// Пример: временное отсутствие соединения с удалённым сервером, не приводящее к критическим сбоям.
        /// </remarks>
        Error,

        /// <summary>
        /// Фатальная ошибка, приводящая к неработоспособности системы или части системы.
        /// </summary>
        Fatal
    }
}
