﻿using C2.DAL;
using C2.Globalization;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YML
{
    class Programm
    {

        public static void Main(string[] args)
        {
            string file = "yml.xml";
            if (args.Length > 0)
            {
                file = args[0];
            }

            string site = "bbsw";
            if (args.Length > 1)
            {
                site = args[1];
            }


            ResourceManager.Init(new FakeResourceProvider(), new List<CultureInfo> { CultureInfo.GetCultureInfo("ru-RU") }, CultureInfo.GetCultureInfo("ru-RU"));

            var s = new Serialization(site);
            
            s.LoadCategory();
            s.LoadOffers();

            s.ToXml(file);

            //Console.ReadKey();
        }
    }
}
