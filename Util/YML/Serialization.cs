﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

using C2.WebApp.Store.Models;

using C2.DataModel.StoreModel;

using C2.DataManager.Store;
using C2.DataMapping.Store;
using C2.AppModel;

using System.Data.Entity;


namespace YML
{


    public class Serialization
    {
        StoreContext _context;
        StoreDataManager _dataManager;

        public Serialization(string site = "bbsw")
        {
            _context = new StoreContext(site == "bbsw" ? "BBSWConnection" : "DefaultConnection");
            _dataManager = new StoreDataManager(_context);

            ycat = new yml_catalog();
            ycat.Shop.Currency.Add(new Currency
            {
                Id = "RUR",
                Rate = 1
            });

            if (site == "bbsw")
            {
                ycat.Shop.Name = "Спорт со вкусом";
                ycat.Shop.Url = "bbsw.ru";
                ycat.Shop.Company = "ООО ВебТек Групп";
            }
            else
            {
                ycat.Shop.Name = "Оптодром";
                ycat.Shop.Url = "optodrom.ru";
                ycat.Shop.Company = "ООО ВебТек Групп";
            }

        }



        public void LoadCategory()
        {
            var _tree = _dataManager.GetProductGroupClassifier();
            ycat.Shop.Categories = new Collection<Category>(
                _tree.AllNodes.Select(n =>
                    new Category
                    {
                        Id = (int)((ProductGroup)n.Value).Id,
                        ParentId = n.Parent != null ? ((ProductGroup)n.Parent.Value).Id.ToString() : null,
                        Name = n.Caption
                    })
                    .Distinct(new CategoryComparer())
                    .ToList());
        }


        string FormatText(string text)
        {
            if (string.IsNullOrEmpty(text)) return "";

            return text
                .Replace("&", "&amp;")
                .Replace(">", "&gt;")
                .Replace("<", "&lt;")
                .Replace("'", "&apos;")
                .Replace("\"", "&quot;");
        }

        public void LoadOffers()
        {
            var products = _context.Products
                .Include(p => p.ProductGroups)
                .Include(p => p.Localization)
                .Include(p => p.Vendor)
                .Include(p => p.Vendor.Localization)
                .Include(p => p.Offers)
                .Include(p => p.Offers.Select(o => o.Unit))
                .Include(p => p.Offers.Select(o => o.Unit.Localization))
                .Include(p => p.MediaCollection)
                .Include(p => p.MediaCollection.Select(m => m.MediaItem))
                .Include(p => p.Offers.Select(o => o.Properties))
                .Include(p => p.Offers.Select(o => o.Properties.Select(pr => pr.PropertyType)))
                .Include(p => p.Offers.Select(o => o.Properties.Select(pr => pr.PropertyType.Localization))).ToList();

            ycat.Shop.offers = new Collection<Offer>();
            foreach (Product p in products)
            {
                foreach (var o in p.Offers)
                {
                    var category = p.ProductGroups.Select(g => new OfferCategory(g.Id)).FirstOrDefault();
                    if (category == null) continue;
                    var newoffer = new Offer
                    {
                        ID = o.Id,
                        Available = o.AvailQuantity > 0,
                        //Category = new Collection<OfferCategory>(p.ProductGroups.Select(g => new OfferCategory(g.Id)).ToArray()),
                        Category = category,
                        Currency = "RUR",
                        Delivery = true,
                        Description = FormatText(p.CurrentLocalization.Description),
                        Pictures = new List<string>(),
                        Price = (int)(o.PriceOpt20.HasValue ? o.Price * (1 - o.PriceOpt20.Value / 100m) : o.Price),
                        Url = "http://" + ycat.Shop.Url + "/Product/Item/" + p.Id,
                        Vendor = FormatText(p.Vendor.CurrentLocalization.Name),
                        TypePrefix = FormatText(p.CurrentLocalization.Title),
                        Model = FormatText(p.CurrentLocalization.Title + " арт. " + p.VendorCode),
                        //Name = FormatText(p.CurrentLocalization.Title),
                        //Country = "Китай",
                        VendorCode = p.VendorCode,
                    };

                    if (p.VendorId == 38802 || p.VendorId == 39102)
                    {
                        newoffer.Adult = "1";
                    }

                    int i = 1;
                    foreach (var pic in p.MediaCollection.OrderByDescending(m => m.MediaItemId).ToList())
                    {
                        if (i++ > 10) { break; }
                        newoffer.Pictures.Add("http://" + ycat.Shop.Url + pic.AsFull());
                    }

                    if (o.Properties.Count > 0)
                    {
                        newoffer.Params = new List<Param>();
                        foreach (var prop in o.Properties)
                        {
                            newoffer.Params.Add(new Param
                            {
                                Name = prop.PropertyType.CurrentLocalization.Title,
                                Value = prop.StringValue,
                            });
                        }
                    }

                    ycat.Shop.offers.Add(newoffer);
                }
            }

        }

        yml_catalog ycat;


        public void ToXml(string file)
        {
            XmlSerializerNamespaces XSN = new XmlSerializerNamespaces();
            XSN.Add("", "");

            XmlWriterSettings XWS = new XmlWriterSettings
            {
                OmitXmlDeclaration = false,
                Encoding = Encoding.UTF8,
            };

            XmlSerializer s = new XmlSerializer(typeof(yml_catalog));
            MemoryStream XmlStr = new MemoryStream();
            s.Serialize(XmlTextWriter.Create(XmlStr, XWS), ycat, XSN);

            XmlStr.Seek(0L, SeekOrigin.Begin);

            using (var reader = new StreamReader(XmlStr))
            {
                File.WriteAllText(file, reader.ReadToEnd());
            }
        }


    }

    class CategoryComparer : IEqualityComparer<Category>
    {
        public bool Equals(Category x, Category y)
        {
            return x.Id == y.Id;
        }

        public int GetHashCode(Category obj)
        {
            if (Object.ReferenceEquals(obj, null)) return 0;
            return obj.Id.GetHashCode();
        }
    }
}
