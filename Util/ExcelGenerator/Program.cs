﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length > 0) { Cfg.Supplier = args[0]; }
            if (args.Length > 1) { Cfg.VendorCode = args.ToList().Skip(1).ToArray(); }

            var excelHelper = new ExcelHelper();
            excelHelper.WriteExcel();

        }
    }
}
