﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DataGenerator
{
    public class Generator
    {
        public static string GetNewCode(int length = 9)
        {
            char[] code = Generate(length);
            string ret = "";
            foreach (char c in code)
            {
                ret += string.Format("{0}", c);
            }

            return string.Join("", code).ToUpper();
        }

        public static char[] Generate(int length = 9)
        {
            char[] random = new char[length*2];

            // генерируем 9 случайных байтов; этого достаточно, чтобы
            // получить 12 случайных символов из набора base64
            byte[] rnd = GenerateRandom(length);

            // конвертируем случайные байты в base64
            Convert.ToBase64CharArray(rnd, 0, rnd.Length, random, 0);

            // очищаем рабочий массив
            Array.Clear(rnd, 0, rnd.Length);

            return random.Take(length).ToArray();
        }

        /// <summary>
        /// Создает массив случайных байтов указанной длины.
        /// </summary>
        /// <param name="size">Требуемая длина массива.</param>
        /// <returns>Массив случайных байтов.</returns>
        private static byte[] GenerateRandom(int size)
        {
            byte[] random = new byte[size];
            RandomNumberGenerator.Create().GetBytes(random);
            return random;
        }

        private int GetCodeIndex(int[] probes, int p)
        {
            int i = 0;

            for (i = 0; i < probes.Length; i++)
            {
                if (p < probes[i])
                {
                    break;
                }
            }
            return i;
        }
    }
}
