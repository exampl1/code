﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2.Globalization;

namespace C2.DataModel
{
    /// <summary>
    /// Афилейт.
    /// </summary>
    public class Affiliate
    {
        public Affiliate()
        {
            AffiliateUsers = new List<Affiliate>();
        }

        /// <summary>
        /// Идентификатор пользователя.
        /// </summary>
        [Key]
        public int UserId { get; set; }

        /// <summary>
        /// Пользователь
        /// </summary>
        // [ForeignKey("UserId")]
        public UserProfile User { get; set; }

        /// <summary>
        /// Уникальный код
        /// </summary>
        public long Code { get; set; }

        /// <summary>
        /// Id, кто привел
        /// </summary>
        public int? ParentUserId { get; set; }

        /// <summary>
        /// пользователи, приведеные данным пользователем 
        /// </summary>
        public List<Affiliate> AffiliateUsers { get; set; }

        public int RegistrationCount { get; set; }
        public int BasketCount { get; set; }
        public decimal BasketSum { get; set; }
        public int PaidCount { get; set; }
        
        /// <summary>
        /// Сумма оплаченных покупок
        /// </summary>
        public decimal PaidSum { get; set; }

        /// <summary>
        /// Сумма использованных бонусов.
        /// </summary>
        public decimal Amount { get; set; }

    }


}
