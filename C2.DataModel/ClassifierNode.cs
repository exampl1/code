﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataModel
{
    /// <summary>
    /// Узел в классификаторе.
    /// </summary>
    public class ClassifierNode : IBaseDataEntity
    {
        public const char PathDelimiter = '/';

        /// <summary>
        /// Идентфикатор.
        /// </summary>
        public long Id
        {
            get;
            set;
        }

        /// <summary>
        /// Состояние.
        /// </summary>
        public EntityStatus Status
        {
            get;
            set;
        }

        /// <summary>
        /// Идентфикатор классификатра.
        /// </summary>
        public long ClassifierId
        {
            get;
            set;
        }

        /// <summary>
        /// Классфикатор.
        /// </summary>
        public Classifier Classifier
        {
            get;
            set;
        }

        /// <summary>
        /// Иерархический путь к узлу от корневого узла.
        /// </summary>
        [Required]
        [MaxLength(MaxLengths.NodePath)]
        public string Path
        {
            get;
            set;
        }

        /// <summary>
        /// Уровень в иерархии.
        /// </summary>
        public int Level
        {
            get;
            set;
        }

        /// <summary>
        /// Порядковый номер.
        /// </summary>
        public int Order
        {
            get;
            set;
        }

        /// <summary>
        /// Необязательный уникальный код узла. Используется для программного поиска.
        /// </summary>
        [MaxLength(MaxLengths.Code)]
        public string Code
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор сущности, связанной с узлом.
        /// </summary>
        public long EntityId
        {
            get;
            set;
        }

        /// <summary>
        /// Признак, что узел является корневым.
        /// </summary>
        /// <returns></returns>
        public bool IsRoot()
        {
            return Level == 0;
        }

        /// <summary>
        /// Установка родительского узла.
        /// </summary>
        /// <param name="parent"></param>
        public void SetParent(ClassifierNode parent = null)
        {
            Path = string.Format("{0}{1:X}{2}", parent != null ? parent.Path : "", Id, PathDelimiter);
            Level = Path.Count(ch => ch == PathDelimiter) - 1;
        }

        /// <summary>
        /// Получение идентфикатора родительского элемента.
        /// </summary>
        /// <returns></returns>
        public long GetParentId()
        {
            if (IsRoot())
            {
                return -1;
            }

            var pathItems = Path.Split(PathSeparators, StringSplitOptions.RemoveEmptyEntries);
            return long.Parse(pathItems[pathItems.Length - 2], NumberStyles.HexNumber);
        }

        static readonly char[] PathSeparators = new char[] { PathDelimiter };
    }
}
