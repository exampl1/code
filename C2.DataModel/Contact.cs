﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2.Globalization;

namespace C2.DataModel
{
    /// <summary>
    /// Контактная информация.
    /// </summary>
    public class Contact : IBaseDataEntity,
        ILocalizable<ContactLocalization>
    {
        public Contact()
        {
            Localization = new List<ContactLocalization>();
        }

        /// <summary>
        /// Идентификатор.
        /// </summary>
        public long Id
        {
            get;
            set;
        }

        /// <summary>
        /// Состояние контакта.
        /// </summary>
        public EntityStatus Status
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор типа контакта.
        /// </summary>
        public long ContactTypeId
        {
            get;
            set;
        }

        /// <summary>
        /// Тип контакта.
        /// </summary>
        [C2Required]
        [Display(ResourceType = typeof(ContactMetaData), Name = "ContactTypeDisplayName")]
        public ContactType ContactType
        {
            get;
            set;
        }

        /// <summary>
        /// Значение.
        /// </summary>
        [C2Required]
        [C2MaxLength(MaxLengths.Contact)]
        [Display(ResourceType = typeof(ContactMetaData), Name = "ValueDisplayName")]
        public string Value
        {
            get;
            set;
        }

        /// <summary>
        /// Локализация.
        /// </summary>
        public ICollection<ContactLocalization> Localization
        {
            get;
            private set;
        }

        [NotMapped]
        public ContactLocalization CurrentLocalization
        {
            get
            {
                return this.GetLocalization();
            }
        }
    }

    /// <summary>
    /// Метаданные для класса Contact.
    /// </summary>
    public static class ContactMetaData
    {
        /// <summary>
        /// Название свойства ContactType.
        /// </summary>
        public static string ContactTypeDisplayName
        {
            get
            {
                return ResourceManager.GetString("Contact.ContactTypeDisplayName");
            }
        }

        /// <summary>
        /// Название свойства Value.
        /// </summary>
        public static string ValueDisplayName
        {
            get
            {
                return ResourceManager.GetString("Contact.ValueDisplayName");
            }
        }
    }
}
