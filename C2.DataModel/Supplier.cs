﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace C2.DataModel
{
    /// <summary>
    /// Субъект.
    /// </summary>
    public class Supplier
    {
        public Supplier()
        {
        }

        /// <summary>
        /// Идентфикатор базового субъекта.
        /// </summary>
        public long SubjectId
        {
            get;
            set;
        }

        /// <summary>
        /// Субъект поставщика
        /// </summary>
        public Subject Subject
        {
            get;
            set;
        }

        /// <summary>
        /// Домен поставщика. Используется для идентификации опций магазина
        /// </summary>
        public string Domain
        {
            get;
            set;
        }
        
        public string YaId
        {
            get;
            set;
        }

        public string PhoneBlock
        {
            get;
            set;
        }

        public string LeftFooterBlock
        {
            get;
            set;
        }

        public string FirstFooterBlock
        {
            get;
            set;
        }

        public string SecondFooterBlock
        {
            get;
            set;
        }

        public string ThirdFooterBlock
        {
            get;
            set;
        }

        public string Logo
        {
            get;
            set;
        }

        public string Banner
        {
            get;
            set;
        }

        public long? PartnerCode
        {
            get;
            set;
        }
    }
}
