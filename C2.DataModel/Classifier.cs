﻿using C2.Globalization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataModel
{
    /// <summary>
    /// Классификатор. Позволяет создавать иерархические структуры.
    /// </summary>
    public class Classifier : IBaseDataEntity,
        ILocalizable<ClassifierLocalization>
    {
        public Classifier()
        {
            Localization = new List<ClassifierLocalization>();
        }

        /// <summary>
        /// Идентификатор.
        /// </summary>
        public long Id
        {
            get;
            set;
        }

        /// <summary>
        /// Состояние.
        /// </summary>
        public EntityStatus Status
        {
            get;
            set;
        }

        /// <summary>
        /// Тип классификатора.
        /// </summary>
        public int ClassifierType
        {
            get;
            set;
        }

        /// <summary>
        /// Код классификатора. Используется для программного поиска.
        /// </summary>
        [C2MaxLength(MaxLengths.Code)]
        [Display(ResourceType = typeof(ClassifierMetaData), Name = "CodeDisplayName")]
        public string Code
        {
            get;
            set;
        }

        /// <summary>
        /// Узлы классификатора.
        /// </summary>
        public ICollection<ClassifierNode> Nodes
        {
            get;
            private set;
        }

        /// <summary>
        /// Локализация.
        /// </summary>
        public ICollection<ClassifierLocalization> Localization
        {
            get;
            private set;
        }

        /// <summary>
        /// Текущая локализация.
        /// </summary>
        public ClassifierLocalization CurrentLocalization
        {
            get
            {
                return this.GetLocalization();
            }
        }
    }

    public static class ClassifierMetaData
    {
        /// <summary>
        /// Название свойства Title.
        /// </summary>
        public static string CodeDisplayName
        {
            get
            {
                return ResourceManager.GetString("Classifier.CodeDisplayName");
            }
        }
    }
}
