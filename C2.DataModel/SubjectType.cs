﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataModel
{
    /// <summary>
    /// Типы субъектов.
    /// </summary>
    public enum SubjectType : byte
    {
        /// <summary>
        /// Персона.
        /// </summary>
        Person,

        /// <summary>
        /// Группа.
        /// </summary>
        Group,

        /// <summary>
        /// Организация.
        /// </summary>
        Organization,

        /// <summary>
        /// Индивидуальный предприниматель
        /// </summary>
        IndividualEntrepreneur,
    }
}
