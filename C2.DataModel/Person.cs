﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace C2.DataModel
{
    /// <summary>
    /// Человек, как частное лицо.
    /// </summary>
    public class Person : ILocalizable<PersonLocalization>
    {
        public Person()
        {
            Localization = new List<PersonLocalization>();
        }

        /// <summary>
        /// Код базового субъекта.
        /// </summary>
        public long SubjectId
        {
            get;
            set;
        }

        /// <summary>
        /// Базовый субъект.
        /// </summary>
        public Subject Subject
        {
            get;
            set;
        }

        /// <summary>
        /// Имя пользователя, ассоциированного с персоной.
        /// </summary>
        [C2MaxLength(MaxLengths.Name)]
        public string UserName
        {
            get;
            set;
        }

        /// <summary>
        /// Пол.
        /// </summary>
        public PersonGender Gender
        {
            get;
            set;
        }

        /// <summary>
        /// Дата рождения.
        /// </summary>
        public DateTime? BirthDate
        {
            get;
            set;
        }

        /// <summary>
        /// Дата смерти.
        /// </summary>
        public DateTime? DeathDate
        {
            get;
            set;
        }

        /// <summary>
        /// Локализация.
        /// </summary>
        public ICollection<PersonLocalization> Localization
        {
            get;
            private set;
        }

        [NotMapped]
        public PersonLocalization CurrentLocalization
        {
            get
            {
                return this.GetLocalization();
            }
        }
    }
}
