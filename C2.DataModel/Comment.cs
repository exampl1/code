﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2.Globalization;

namespace C2.DataModel
{
    /// <summary>
    /// Комментарий.
    /// </summary>
    public class Comment : IBaseDataEntity
    {
        /// <summary>
        /// Идентфикатор.
        /// </summary>
        public long Id
        {
            get;
            set;
        }


        /// <summary>
        /// Статус.
        /// </summary>
        public EntityStatus Status
        {
            get;
            set;
        }

        /// <summary>
        /// Имя пользователя, оставившего комментарий.
        /// </summary>
        [Display(ResourceType = typeof(CommentMetaData), Name = "UserNameDisplayName")]
        public string UserName
        {
            get;
            set;
        }

        [Display(ResourceType = typeof(CommentMetaData), Name = "UserDisplayNameDisplayName")]
        [MaxLength(MaxLengths.Name)]
        [Required(ErrorMessage="не может быть пустым")]
        public string UserDisplayName
        {
            get;
            set;
        }
        

        /// <summary>
        /// Дата создания комментария.
        /// </summary>
        [Display(ResourceType = typeof(CommentMetaData), Name = "CreatedDisplayName")]
        public DateTime Created
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор узла иерархических комментариев.
        /// </summary>
        [MaxLength(MaxLengths.NodePath)]
        public string Path
        {
            get;
            set;
        }

        /// <summary>
        /// Текст комментария.
        /// </summary>
        [Required(ErrorMessage = "не может быть пустым")]
        [MaxLength(MaxLengths.Comment)]
        [DataType(DataType.MultilineText)]
        [Display(ResourceType = typeof(CommentMetaData), Name = "TextDisplayName")]
        public string Text
        {
            get;
            set;
        }
    }

    /// <summary>
    /// Метаданные для класса Comment.
    /// </summary>
    public static class CommentMetaData
    {
        /// <summary>
        /// Название для свойства User.
        /// </summary>
        public static string UserDisplayNameDisplayName
        {
            get
            {
                return ResourceManager.GetString("Comment.UserDisplayNameDisplayName");
            }
        }

        public static string UserNameDisplayName
        {
            get
            {
                return ResourceManager.GetString("Comment.UserNameDisplayName");
            }
        }

        /// <summary>
        /// Название для свойства Created.
        /// </summary>
        public static string CreatedDisplayName
        {
            get
            {
                return ResourceManager.GetString("Comment.CreatedDisplayName");
            }
        }

        /// <summary>
        /// Название для свойства Name.
        /// </summary>
        public static string TextDisplayName
        {
            get
            {
                return ResourceManager.GetString("Comment.TextDisplayName");
            }
        }
    }
}
