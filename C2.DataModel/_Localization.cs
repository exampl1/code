﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2.Globalization;

namespace C2.DataModel
{
    /// <summary>
    /// Сущность, поддерживающая локализацию.
    /// </summary>
    /// <typeparam name="TLocalization"></typeparam>
    public interface ILocalizable<TLocalization>
        where TLocalization: ILocalization
    {
        ICollection<TLocalization> Localization
        {
            get;
        }

        TLocalization CurrentLocalization
        {
            get;
        }
    }

    /// <summary>
    /// Локализация сущности.
    /// </summary>
    public interface ILocalization
    {
        string CultureCode
        {
            get;
        }
    }

    /// <summary>
    /// Вспомогательный класс локализации.
    /// </summary>
    public static class LocalsHelper
    {
        /// <summary>
        /// Получение информации о культуре локализации.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static CultureInfo GetCulture(this ILocalization obj)
        {
            return CultureInfo.GetCultureInfo(obj.CultureCode);
        }

        /// <summary>
        /// Получение локализации по коду культуры.
        /// </summary>
        /// <typeparam name="TLocals"></typeparam>
        /// <param name="obj"></param>
        /// <param name="cultureCode">Код культуры.</param>
        /// <returns></returns>
        /// <remarks>
        /// Порядок поиска: культура - родительская культура - культура по умолчанию.
        /// Если ни одно из условий не выполнено, то возвращается первая локализация из списка.
        /// </remarks>
        public static TLocals GetLocalization<TLocals>(this ILocalizable<TLocals> obj,
            string cultureCode)
            where TLocals : ILocalization
        {
            if ((obj.Localization == null) || (obj.Localization.Count == 0))
            {
                return default(TLocals);
            }

            if (obj.Localization.Count == 1)
            {
                return obj.Localization.First();
            }

            // Пробуем найти для основной культуры.
            var locals = obj.Localization.FirstOrDefault(l => l.CultureCode == cultureCode);
            if (locals != null)
            {
                return locals;
            }

            if (cultureCode.Contains('-'))
            {
                // Пробуем найти локализацию для родительской культуры.
                cultureCode = cultureCode.Split(_cultureSplitter, 1)[0];
                locals = obj.Localization.FirstOrDefault(l => l.CultureCode == cultureCode);
                if (locals != null)
                {
                    return locals;
                }
            }

            // Пробуем найти для культуры по умолчанию.
            cultureCode = ResourceManager.DefaultCulture.TwoLetterISOLanguageName;
            locals = obj.Localization.FirstOrDefault(l => l.CultureCode == cultureCode);
            if (locals != null)
            {
                return locals;
            }

            return obj.Localization.First();
        }

        /// <summary>
        /// Получение локализации для текущей культуры UI.
        /// </summary>
        /// <typeparam name="TLocals"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static TLocals GetLocalization<TLocals>(this ILocalizable<TLocals> obj)
            where TLocals : ILocalization
        {
            return obj.GetLocalization<TLocals>(CultureInfo.CurrentUICulture.TwoLetterISOLanguageName);
        }

        static readonly char[] _cultureSplitter = new char[] { '-' };
    }
}
