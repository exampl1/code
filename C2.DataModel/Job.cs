﻿using C2.Globalization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataModel
{
    /// <summary>
    /// Классификатор. Позволяет создавать иерархические структуры.
    /// </summary>
    public class Job : IBaseDataEntity
    {
        public Job()
        {
        }

        /// <summary>
        /// Идентификатор.
        /// </summary>
        public long Id
        {
            get;
            set;
        }

        /// <summary>
        /// Состояние.
        /// </summary>
        public EntityStatus Status
        {
            get;
            set;
        }

        public DateTime StartDate
        {
            get;
            set;
        }

        public string TemplateName
        {
            get;
            set;
        }

    }

    
}
