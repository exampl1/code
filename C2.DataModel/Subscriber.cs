﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataModel
{
    public class Subscriber
    {
        public long Id { get; set; }

        [Required(ErrorMessage = "Укажите {0}.")]
        [RegularExpression(@"^\S+@\S+\.\S+$", ErrorMessage = "Нужно ввести реальный email")]
        public string Email { get; set; }

        [Display(Name = "Имя")]
        [Required(ErrorMessage = "Укажите {0}.")]
        [MinLength(3, ErrorMessage = "Очень короткое {0}.")]
        public string Name { get; set; }

        public DateTime Created { get; set; }
    }
}
