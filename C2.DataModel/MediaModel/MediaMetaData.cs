﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataModel.MediaModel
{
    public interface IMediaMetaData
    {
    }

    public interface IVisualMetaData : IMediaMetaData
    {
        int Height
        {
            get;
            set;
        }

        int Width
        {
            get;
            set;
        }
    }

    /// <summary>
    /// Метаданные для изображения.
    /// </summary>
    public class PictureMetaData : IVisualMetaData
    {
        /// <summary>
        /// Тип изображения (контекстно-зависимый).
        /// </summary>
        public string PictureType
        {
            get;
            set;
        }

        /// <summary>
        /// Высота изображения (точек).
        /// </summary>
        public int Height
        {
            get;
            set;
        }

        /// <summary>
        /// Ширина изображения (точек).
        /// </summary>
        public int Width
        {
            get;
            set;
        }
    }
}
