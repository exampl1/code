﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace BaseDataModel.MediaModel
//{
//    public class MediaCollection : BaseObject,
//        ILocalizableObject<MediaCollectionLocalization>,
//        ITaggable
//    {
//        public MediaCollection()
//        {
//            Localizations = new List<MediaCollectionLocalization>();
//            Compilers = new List<Subject>();
//        }

//        public List<MediaCollectionLocalization> Localizations
//        {
//            get;
//            private set;
//        }

//        // Many-To-Many
//        public List<Subject> Compilers
//        {
//            get;
//            private set;
//        }

//        public DateTime CreateDate
//        {
//            get;
//            set;
//        }

//        public DateTime? PublishDate
//        {
//            get;
//            set;
//        }

//        public string Tags
//        {
//            get;
//            set;
//        }
//    }

//    public class MediaCollectionLocalization : ObjectLocalization
//    {
//        public string Title
//        {
//            get;
//            set;
//        }

//        public string Description
//        {
//            get;
//            set;
//        }
//    }
//}
