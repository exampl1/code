﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataModel.MediaModel
{
    public class MediaItemLocalization : ILocalization
    {
        [Required]
        [MaxLength(MaxLengths.CultureCode)]
        public string CultureCode
        {
            get;
            set;
        }

        public long MediaItemId
        {
            get;
            set;
        }

        public MediaItem MediaItem
        {
            get;
            set;
        }

        [Required]
        [MaxLength(MaxLengths.Title)]
        public string Title
        {
            get;
            set;
        }

        [MaxLength(MaxLengths.Description)]
        public string Description
        {
            get;
            set;
        }
    }
}
