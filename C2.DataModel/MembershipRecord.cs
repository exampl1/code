﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace C2.DataModel
{
    [Table("webpages_Membership")]
    public class MembershipRecord
    {
        [Key]
        public int UserId { get; set; }

        [Display(Name = "Дата регистрации")]
        public DateTime CreateDate { get; set; }

        [Display(Name = "Подтвержден")]
        public bool IsConfirmed { get; set; }

        [Display(Name = "Дата смены пароля")]
        public DateTime PasswordChangedDate { get; set; }

        public UserProfile User { get; set; }
    }
}
