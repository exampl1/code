﻿CREATE TABLE [dbo].[Visitor] (
    [Id]     [dbo].[EntityId]           IDENTITY (1, 1) NOT NULL,
    [Name]   NVARCHAR (256) NULL,
    [Email]  VARCHAR (128) NOT NULL,
    [Status] INT           DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Visitor] PRIMARY KEY CLUSTERED ([Id] ASC)
);

