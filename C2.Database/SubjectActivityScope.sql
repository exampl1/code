﻿CREATE TABLE [dbo].[SubjectActivityScope]
(
	[SubjectId] [dbo].[EntityId] NOT NULL, 
    [ActivityId] [dbo].[EntityId] NOT NULL, 
    CONSTRAINT [PK_SubjectActivityScope] PRIMARY KEY ([SubjectId], [ActivityId]), 
    CONSTRAINT [FK_SubjectActivityScope_ToSubject] FOREIGN KEY ([SubjectId]) REFERENCES [Subject]([Id]), 
    CONSTRAINT [FK_SubjectActivityScope_ToActivityDictionary] FOREIGN KEY ([ActivityId]) REFERENCES [DictionaryItem]([Id])
)
