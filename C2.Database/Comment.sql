﻿CREATE TABLE [dbo].[Comment]
(
	[Id] [dbo].[EntityId] NOT NULL, 
    [Status] INT NOT NULL, 
	[EntityId] [dbo].[EntityId] NULL,
    [UserName] [dbo].[UserName] NOT NULL, 
    [Created] DATETIME NOT NULL, 
    [Path] [dbo].[NodePath] NULL, 
    [Text] [dbo].[Description] NOT NULL, 
    [UserDisplayName] [dbo].[UserName] NOT NULL, 
    CONSTRAINT [PK_Comment] PRIMARY KEY ([Id])
)
GO

CREATE INDEX [IX_Comment_EntityId] ON [dbo].[Comment] ([EntityId])

GO

CREATE INDEX [IX_Comment_Path] ON [dbo].[Comment] ([Path])
