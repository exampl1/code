﻿CREATE TABLE [dbo].[MediaItem]
(
	[Id] [dbo].[EntityId] NOT NULL, 
    [Status] INT NOT NULL, 
    [MediaType] INT NOT NULL, 
    [MediaProvider] [dbo].[Code] NULL, 
    [PermaLink] [dbo].[Url] NOT NULL, 
    [Data] NVARCHAR(MAX) NULL, 
    CONSTRAINT [PK_MediaItem] PRIMARY KEY ([Id])
)
