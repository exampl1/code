﻿CREATE TABLE [Security].[Membership]
(
	[UserName] [dbo].[UserName] NOT NULL , 
    [RoleId] [dbo].[EntityId] NOT NULL, 
    [Level] TINYINT NOT NULL, 
    [Status] INT NOT NULL, 
    CONSTRAINT [PK_Membership] PRIMARY KEY ([UserName], [RoleId]), 
    CONSTRAINT [FK_Membership_ToRole] FOREIGN KEY ([RoleId]) REFERENCES [Security].[Role]([Id])
)
