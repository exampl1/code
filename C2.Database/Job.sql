﻿CREATE TABLE [dbo].[Job]
(
	[Id] [dbo].[EntityId] NOT NULL PRIMARY KEY, 
    [StartDate] DATETIME NOT NULL DEFAULT getdate(), 
    [TemplateName] NVARCHAR(256) NOT NULL, 
    [Status] INT NOT NULL DEFAULT 0
)
