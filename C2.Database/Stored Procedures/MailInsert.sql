CREATE procedure 
[dbo].[MailInsert] @FromEmail nvarchar(256),
@ToEmail nvarchar(128),
@ToUserId int,
@Subject nvarchar(1024),
@Body nvarchar(max),
@IsBodyHtml bit,
@Priority tinyint,
@Type int,
@Status tinyint,
@ApplicationName nvarchar(50) = null
as begin
DECLARE @NowDate datetime;
SET @NowDate = getutcdate();

	DECLARE @SiteName nvarchar(50);
	/*SELECT @SiteName = SiteName FROM dbo.tblApplication WITH(NOLOCK) WHERE ApplicationName = @ApplicationName;
	IF @SiteName IS NULL SET @SiteName = 'key2business.ru';
	SET @Body = REPLACE(@Body,'[Site]', @SiteName);
	SET @Subject = REPLACE(@Subject,'[Site]', @SiteName);*/


INSERT INTO dbo.MailQueue WITH(UPDLOCK)
                      (ToEmail, ToUserId, Priority, [Type], [Subject], Body, [Status], FromEmail, IsBodyHtml, Created)
VALUES     (@ToEmail,@ToUserId,@Priority,@Type,@Subject,@Body,@Status,@FromEmail,@IsBodyHtml, @NowDate)
end
