﻿CREATE TABLE [dbo].[ClassifierLocalization]
(
	[ClassifierId] [dbo].[EntityId] NOT NULL, 
    [CultureCode] [dbo].[CultureCode] NOT NULL, 
    [Title] [dbo].[Title] NOT NULL, 
    [Description] [dbo].[Description] NULL, 
    CONSTRAINT [PK_ClassifierLocalization] PRIMARY KEY ([ClassifierId], [CultureCode]), 
    CONSTRAINT [FK_ClassifierLocalization_ToClassifier] FOREIGN KEY ([ClassifierId]) REFERENCES [Classifier]([Id]) 
)
