﻿CREATE TABLE [dbo].[PropertyValue]
(
	[Id] [dbo].[EntityId] NOT NULL,
    [Status] INT NOT NULL,
    [PropertyTypeId] [dbo].[EntityId] NOT NULL,
    [ByteValue] TINYINT NULL,
    [IntValue] BIGINT NULL,
    [DecimalValue] DECIMAL(18, 6) NULL,
    [DateValue] DATETIME NULL,
    [StringValue] NVARCHAR(MAX) NULL,
    CONSTRAINT [PK_PropertyValue] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_PropertyValue_ToPropertyTypeDictionary] FOREIGN KEY ([PropertyTypeId]) REFERENCES [DictionaryItem]([Id])
)
