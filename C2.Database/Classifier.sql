﻿CREATE TABLE [dbo].[Classifier]
(
	[Id] [dbo].[EntityId] NOT NULL,
	[Status] INT NOT NULL,
    [ClassifierType] INT NOT NULL,
    [Code] [dbo].[Code] NULL,
    CONSTRAINT [PK_Classifier] PRIMARY KEY ([Id])
)

GO

CREATE UNIQUE INDEX [IX_Classifier_Code] ON [dbo].[Classifier] ([Code])
