﻿CREATE TYPE [dbo].[Name]
	FROM nvarchar(50)

GO

CREATE TYPE [dbo].[FullName]
	FROM nvarchar(200)
