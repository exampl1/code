﻿CREATE TYPE [dbo].[Quantity]
	FROM decimal(18,6) NOT NULL
