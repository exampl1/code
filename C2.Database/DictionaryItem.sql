﻿CREATE TABLE [dbo].[DictionaryItem]
(
	[Id] [dbo].[EntityId] NOT NULL,
	[DictType] INT NOT NULL,
    [Status] INT NOT NULL,
    [Code] [dbo].[Code] NULL,
	[Flags] INT NOT NULL DEFAULT 0,
    [Data] NVARCHAR(MAX) NULL,
    CONSTRAINT [PK_DictionaryItem] PRIMARY KEY ([Id])
)

GO
