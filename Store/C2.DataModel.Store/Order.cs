﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataModel.StoreModel
{
    /// <summary>
    /// Заказ.
    /// </summary>
    public class Order : IBaseDataEntity
    {
        public Order()
        {
            Items = new List<OrderItem>();
        }

        /// <summary>
        /// Идентификатор.
        /// </summary>
        public long Id
        {
            get;
            set;
        }

        /// <summary>
        /// Состояние.
        /// </summary>
        public EntityStatus Status
        {
            get;
            set;
        }

        /// <summary>
        /// Состояние заказа.
        /// </summary>
        [Display(Name = "Состояние")]
        public OrderStatus OrderStatus
        {
            get;
            set;
        }

        /// <summary>
        /// Идентифкатор клиента.
        /// </summary>
        [Display(Name = "ID заказчика")]
        public string CustomerId
        {
            get;
            set;
        }

        /// <summary>
        /// Профиль клиента.
        /// </summary>
        [Display(Name = "Заказчик")]
        public UserProfile UserProfile
        {
            get;
            set;
        }

        /// <summary>
        /// Доставка.
        /// </summary>
        //[Display(Name = "Доставка")]
        //public OrderDelivery OrderDelivery
        //{
        //    get;
        //    set;
        //}

        /// <summary>
        /// Идентифкатор плательщика.
        /// </summary>
        public string PayerId
        {
            get;
            set;
        }

        /// <summary>
        /// Дата создания.
        /// </summary>
        [Display(Name = "Дата создания")]
        public DateTime CreateDate
        {
            get;
            set;
        }

        /// <summary>
        /// Дата оплаты (полной).
        /// </summary>
        public DateTime? PayDate
        {
            get;
            set;
        }

        /// <summary>
        /// Дата завершения выполения заказа.
        /// </summary>
        public DateTime? CompleteDate
        {
            get;
            set;
        }

        /// <summary>
        /// Код валюты заказа.
        /// </summary>
        public string CurrencyCode
        {
            get;
            set;
        }

        /// <summary>
        /// Промежуточный итог.
        /// </summary>
        public decimal SubTotal
        {
            get;
            set;
        }

        /// <summary>
        /// Сумма скидок.
        /// </summary>
        public decimal DiscountTotal
        {
            get;
            set;
        }

        /// <summary>
        /// Сумма налогов.
        /// </summary>
        public decimal TaxTotal
        {
            get;
            set;
        }

        /// <summary>
        /// Итог.
        /// </summary>
        [Display(Name = "К оплате всего")]
        public decimal Total
        {
            get;
            set;
        }

        /// <summary>
        /// Суммарная оплата, поступившая в счёт заказа.
        /// </summary>
        [Display(Name = "Оплачено всего")]
        public decimal TotalPayment
        {
            get;
            set;
        }

        /// <summary>
        /// Информация о доставке.
        /// </summary>
        [Display(Name = "Доставка")]
        public string Delivery
        {
            get;
            set;
        }

        [Display(Name = "Предзаказ")]
        public bool IsPreorder
        {
            get;
            set;
        }
        
        /// <summary>
        /// Элементы заказа.
        /// </summary>
        public List<OrderItem> Items
        {
            get;
            private set;
        }

        public string AdminComment
        {
            get;
            set;
        }
        
        [Display(Name = "Комментарий к заказу")]
        public string UserComment
        {
            get;
            set;
        }

        public bool IsEmpty()
        {
            return (Items == null) || (Items.Count == 0);
        }

        public void UpdateTotals(bool updateAllItems = true, bool isWholesale = true)
        {
            if (updateAllItems)
            {
                Items.ForEach(i => i.UpdateAll());
            }

            SubTotal = Items.Sum(i => i.SubTotal);
            DiscountTotal = Items.Sum(i => i.DiscountTotal);
            TaxTotal = Items.Sum(i => i.TaxTotal);
            Total = SubTotal - DiscountTotal + TaxTotal;

            if (UserProfile != null && UserProfile.Affiliate != null)
            {
                UserProfile.Affiliate.BasketCount = isWholesale ? 0 : (int)Items.Sum(i => i.Quantity);
                UserProfile.Affiliate.BasketSum = isWholesale ? 0 : Total;
            }
        }

    }
}
