﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataModel.StoreModel
{
    /// <summary>
    /// Группа товаров.
    /// </summary>
    public class ProductInGroup
    {
        public long ProductGroupId { get; set; }
        public long ProductId { get; set; }

        public Product Product { get; set; }
        public ProductGroup ProductGroup { get; set; }
    }
}
