﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2.DataModel.MediaModel;
using C2.Globalization;
using System.ComponentModel.DataAnnotations;

namespace C2.DataModel.StoreModel
{
    [Table("TopProduct")]
    public class TopProduct
    {
        public TopProduct()
        {
        }

        /// <summary>
        /// Идентификатор.
        /// </summary>
        [Key]
        public long ProductId
        {
            get;
            set;
        }

        /// <summary>
        /// Тип.
        /// </summary>
        public int Type
        {
            get;
            set;
        }

    }
}
