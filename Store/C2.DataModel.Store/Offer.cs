﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataModel.StoreModel
{
    /// <summary>
    /// Товарное предложение.
    /// </summary>
    public class Offer : IBaseDataEntity
    {
        public Offer()
        {
            Properties = new List<PropertyValue>();
        }

        /// <summary>
        /// Идентифкатор.
        /// </summary>
        public long Id
        {
            get;
            set;
        }

        /// <summary>
        /// Состояние.
        /// </summary>
        public EntityStatus Status
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор поставщика.
        /// </summary>
        public long SupplierId
        {
            get;
            set;
        }

        /// <summary>
        /// Поставщик.
        /// </summary>
        public Supplier Supplier
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор товара.
        /// </summary>
        public long ProductId
        {
            get;
            set;
        }

        /// <summary>
        /// Товар.
        /// </summary>
        public Product Product
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор единицы измерения.
        /// </summary>
        public long UnitId
        {
            get;
            set;
        }

        /// <summary>
        /// Единица измерения.
        /// </summary>
        public Unit Unit
        {
            get;
            set;
        }

        /// <summary>
        /// Код валюты.
        /// </summary>
        public string CurrencyCode
        {
            get;
            set;
        }

        /// <summary>
        /// Цена.
        /// </summary>
        public decimal Price
        {
            get;
            set;
        }

        /// <summary>
        /// Цена оптовая -20% от розницы.
        /// </summary>
        public decimal? PriceOpt20
        {
            get;
            set;
        }

        /// <summary>
        /// Цена оптовая -40% от розницы.
        /// </summary>
        public decimal? PriceOpt40
        {
            get;
            set;
        }

        /// <summary>
        /// Дата начала действия предложения.
        /// </summary>
        public DateTime StartDate
        {
            get;
            set;
        }

        /// <summary>
        /// Дата окончания действия предложения.
        /// </summary>
        public DateTime ExpiredDate
        {
            get;
            set;
        }

        /// <summary>
        /// Доступное колличество
        /// </summary>
        public decimal AvailQuantity 
        { 
            get; 
            set; 
        }

        public decimal PreorderQuantity
        {
            get;
            set;
        }

        /// <summary>
        /// Ключевые свойства предложения.
        /// </summary>
        public ICollection<PropertyValue> Properties
        {
            get;
            private set;
        }
    }
}
