﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(C2.WebApp.Store.Mishka.Startup))]
namespace C2.WebApp.Store.Mishka
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
