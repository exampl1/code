﻿using C2.DataManager.Store;
using C2.DataMapping.Store;
using C2.WebApp.Store.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using C2.DataModel.StoreModel;
using C2.DataModel;

namespace C2.WebApp.Store.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        public ActionResult Index()
        {
            var productModel = ViewData.GetPageModel();

            var topProducts = _dataContext.TopProducts.ToList();

            var result = _dataManager.SelectProducts(new SelectProductParams { PageSize = 16, OrderBy = "Order" });
            productModel.SelectResult = new DAL.SelectResult<Product>(result.Items.Take(4).ToList());

            productModel.Select1Result = new DAL.SelectResult<Product>(result.Items.Skip(4).ToList());

            productModel.PageContent = _dataManager.GetContent("home");

            return View(productModel);
        }


        public ActionResult Landing(string sid)
        {
            ViewBag.ShowForm = true;
            if (sid != null)
            {
                _dataManager.FireSid(new Guid(sid));
            }
            return View(_dataManager.GetContent("landing"));
        }

        [HttpPost]
        public ActionResult Landing(C2.DataModel.Subscriber model)
        {
            if (ModelState.IsValid)
            {
                _dataManager.SetupSubscriber(model);
                ViewBag.ShowForm = false;
                return View(_dataManager.GetContent("landing-ok"));
            }

            ViewBag.ShowForm = true;
            ModelState.AddModelError("", new Exception("Форма заполнена неверно"));
            return View(_dataManager.GetContent("landing"));
        }

        //
        // GET: /Home/Get
        public ActionResult Get(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                var content = _dataManager.GetContent(id);
                if (content != null)
                {
                    return View("Get", content);
                }
            }

            throw new HttpException(404, string.Format("Страница {0} не найдена", id));
        }


        public HomeController()
        {
            _dataContext = new StoreContext("DefaultConnection");
            _dataManager = new StoreDataManager(_dataContext);
        }

        StoreContext _dataContext;

        StoreDataManager _dataManager;

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _dataContext.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}
