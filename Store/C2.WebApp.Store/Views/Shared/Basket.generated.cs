﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18034
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace C2.WebApp.Store.Views.Shared
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using System.Web.Mvc.Ajax;
    using System.Web.Mvc.Html;
    using System.Web.Optimization;
    using System.Web.Routing;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.WebPages;
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("RazorGenerator", "1.5.0.0")]
    [System.Web.WebPages.PageVirtualPathAttribute("~/Views/Shared/Basket.cshtml")]
    public class Basket : System.Web.Mvc.WebViewPage<C2.DataModel.StoreModel.Order>
    {
        public Basket()
        {
        }
        public override void Execute()
        {
WriteLiteral("В корзине ??????????:\r\n\r\n<table>\r\n");

            
            #line 6 "..\..\Views\Shared\Basket.cshtml"
 foreach (var item in Model.Items)
{

            
            #line default
            #line hidden
WriteLiteral("    <tr>\r\n        <td>");

            
            #line 9 "..\..\Views\Shared\Basket.cshtml"
       Write(item.Offer.Product.CurrentLocalization.Title);

            
            #line default
            #line hidden
WriteLiteral("</td>\r\n        <td>");

            
            #line 10 "..\..\Views\Shared\Basket.cshtml"
       Write(item.Quantity);

            
            #line default
            #line hidden
WriteLiteral("</td>\r\n        <td>");

            
            #line 11 "..\..\Views\Shared\Basket.cshtml"
       Write(item.Offer.Unit.CurrentLocalization.Title);

            
            #line default
            #line hidden
WriteLiteral("</td>\r\n        <td>");

            
            #line 12 "..\..\Views\Shared\Basket.cshtml"
       Write(item.Total);

            
            #line default
            #line hidden
WriteLiteral("</td>\r\n        <td></td>\r\n    </tr>\r\n");

            
            #line 15 "..\..\Views\Shared\Basket.cshtml"
}

            
            #line default
            #line hidden
WriteLiteral("</table>\r\n\r\n<div");

WriteLiteral(" class=\"total\"");

WriteLiteral(">\r\nИтого: ");

            
            #line 19 "..\..\Views\Shared\Basket.cshtml"
  Write(Model.Total);

            
            #line default
            #line hidden
WriteLiteral("\r\n</div>\r\n\r\n");

            
            #line 22 "..\..\Views\Shared\Basket.cshtml"
 if (!Model.IsEmpty())
{

            
            #line default
            #line hidden
WriteLiteral("    <div");

WriteLiteral(" class=\"create\"");

WriteLiteral(">Оформить заказ</div>\r\n");

            
            #line 25 "..\..\Views\Shared\Basket.cshtml"
}

            
            #line default
            #line hidden
        }
    }
}
#pragma warning restore 1591
