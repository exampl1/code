﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18034
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace C2.WebApp.Store.Views.Shared
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using System.Web.Mvc.Ajax;
    using System.Web.Mvc.Html;
    using System.Web.Optimization;
    using System.Web.Routing;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.WebPages;
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("RazorGenerator", "1.5.0.0")]
    [System.Web.WebPages.PageVirtualPathAttribute("~/Views/Shared/_CustomerInfo.cshtml")]
    public class CustomerInfo : System.Web.Mvc.WebViewPage<C2.DataModel.StoreModel.CustomerInfo>
    {
        public CustomerInfo()
        {
        }
        public override void Execute()
        {
WriteLiteral("<div");

WriteLiteral(" id=\"update-panel\"");

WriteLiteral(">\r\n<div");

WriteLiteral(" class=\"col-title\"");

WriteLiteral(">");

            
            #line 3 "..\..\Views\Shared\_CustomerInfo.cshtml"
                  Write(Html.LabelFor(m => m.ContactName));

            
            #line default
            #line hidden
WriteLiteral("</div>\r\n<div");

WriteLiteral(" class=\"col-control\"");

WriteLiteral(">");

            
            #line 4 "..\..\Views\Shared\_CustomerInfo.cshtml"
                    Write(Html.EditorFor(m => m.ContactName));

            
            #line default
            #line hidden
WriteLiteral("</div>\r\n<div");

WriteLiteral(" class=\"row-split\"");

WriteLiteral("></div>\r\n\r\n<div");

WriteLiteral(" class=\"col-title\"");

WriteLiteral(">");

            
            #line 7 "..\..\Views\Shared\_CustomerInfo.cshtml"
                  Write(Html.LabelFor(m => m.Email));

            
            #line default
            #line hidden
WriteLiteral("</div>\r\n<div");

WriteLiteral(" class=\"col-control\"");

WriteLiteral(">");

            
            #line 8 "..\..\Views\Shared\_CustomerInfo.cshtml"
                    Write(Html.EditorFor(m => m.Email));

            
            #line default
            #line hidden
WriteLiteral("</div>\r\n<div");

WriteLiteral(" class=\"row-split\"");

WriteLiteral("></div>\r\n\r\n<div");

WriteLiteral(" class=\"col-title\"");

WriteLiteral(">");

            
            #line 11 "..\..\Views\Shared\_CustomerInfo.cshtml"
                  Write(Html.LabelFor(m => m.Phone));

            
            #line default
            #line hidden
WriteLiteral("</div>\r\n<div");

WriteLiteral(" class=\"col-control\"");

WriteLiteral(">");

            
            #line 12 "..\..\Views\Shared\_CustomerInfo.cshtml"
                    Write(Html.EditorFor(m => m.Phone));

            
            #line default
            #line hidden
WriteLiteral("</div>\r\n<div");

WriteLiteral(" class=\"row-split\"");

WriteLiteral("></div>\r\n\r\n<div");

WriteLiteral(" class=\"col-title\"");

WriteLiteral(">");

            
            #line 15 "..\..\Views\Shared\_CustomerInfo.cshtml"
                  Write(Html.LabelFor(m => m.Organization));

            
            #line default
            #line hidden
WriteLiteral("</div>\r\n<div");

WriteLiteral(" class=\"col-control\"");

WriteLiteral(">\r\n");

WriteLiteral("    ");

            
            #line 17 "..\..\Views\Shared\_CustomerInfo.cshtml"
Write(Html.EditorFor(m => m.Organization));

            
            #line default
            #line hidden
WriteLiteral(@"
    <div><em>полное название в соответствии с учредительными документами. Включает в себя организационно-правовую форму и название юридического лица или ИП без использования аббревиатур. Используется при формировании счетов на оплату.</em></div>
</div>
<div");

WriteLiteral(" class=\"row-split\"");

WriteLiteral("></div>\r\n\r\n<div");

WriteLiteral(" class=\"col-title\"");

WriteLiteral(">");

            
            #line 22 "..\..\Views\Shared\_CustomerInfo.cshtml"
                  Write(Html.LabelFor(m => m.INN));

            
            #line default
            #line hidden
WriteLiteral("</div>\r\n<div");

WriteLiteral(" class=\"col-control\"");

WriteLiteral(">");

            
            #line 23 "..\..\Views\Shared\_CustomerInfo.cshtml"
                    Write(Html.EditorFor(m => m.INN));

            
            #line default
            #line hidden
WriteLiteral("</div>\r\n<div");

WriteLiteral(" class=\"row-split\"");

WriteLiteral("></div>\r\n\r\n<div");

WriteLiteral(" class=\"col-title\"");

WriteLiteral(">");

            
            #line 26 "..\..\Views\Shared\_CustomerInfo.cshtml"
                  Write(Html.LabelFor(m => m.OGRN));

            
            #line default
            #line hidden
WriteLiteral("</div>\r\n<div");

WriteLiteral(" class=\"col-control\"");

WriteLiteral(">");

            
            #line 27 "..\..\Views\Shared\_CustomerInfo.cshtml"
                    Write(Html.EditorFor(m => m.OGRN));

            
            #line default
            #line hidden
WriteLiteral("\r\n    <div><em>Для организаций ОГРН, для ИП - ОГРНИП</em></div>\r\n</div>\r\n<div");

WriteLiteral(" class=\"row-split\"");

WriteLiteral("></div>\r\n\r\n<div");

WriteLiteral(" class=\"col-title\"");

WriteLiteral(">");

            
            #line 32 "..\..\Views\Shared\_CustomerInfo.cshtml"
                  Write(Html.LabelFor(m => m.KPP));

            
            #line default
            #line hidden
WriteLiteral("</div>\r\n<div");

WriteLiteral(" class=\"col-control\"");

WriteLiteral(">\r\n");

WriteLiteral("    ");

            
            #line 34 "..\..\Views\Shared\_CustomerInfo.cshtml"
Write(Html.EditorFor(m => m.KPP));

            
            #line default
            #line hidden
WriteLiteral("\r\n    <div><em>Обязательно для организаций</em></div>\r\n</div>\r\n<div");

WriteLiteral(" class=\"row-split\"");

WriteLiteral("></div>\r\n\r\n<div");

WriteLiteral(" class=\"col-title\"");

WriteLiteral(">");

            
            #line 39 "..\..\Views\Shared\_CustomerInfo.cshtml"
                  Write(Html.LabelFor(m => m.Requisites));

            
            #line default
            #line hidden
WriteLiteral("</div>\r\n<div");

WriteLiteral(" class=\"col-control\"");

WriteLiteral(">");

            
            #line 40 "..\..\Views\Shared\_CustomerInfo.cshtml"
                    Write(Html.EditorFor(m => m.Requisites));

            
            #line default
            #line hidden
WriteLiteral("</div>\r\n<div");

WriteLiteral(" class=\"row-split\"");

WriteLiteral("></div>\r\n\r\n<h3>Адрес доставки</h3>\r\n\r\n<div");

WriteLiteral(" class=\"col-title\"");

WriteLiteral(">");

            
            #line 45 "..\..\Views\Shared\_CustomerInfo.cshtml"
                  Write(Html.LabelFor(m => m.DeliveryAddress.PostCode));

            
            #line default
            #line hidden
WriteLiteral("</div>\r\n<div");

WriteLiteral(" class=\"col-control\"");

WriteLiteral(">");

            
            #line 46 "..\..\Views\Shared\_CustomerInfo.cshtml"
                    Write(Html.EditorFor(m => m.DeliveryAddress.PostCode));

            
            #line default
            #line hidden
WriteLiteral(" ");

            
            #line 46 "..\..\Views\Shared\_CustomerInfo.cshtml"
                                                                     Write(Html.ValidationMessageFor(m => m.DeliveryAddress.PostCode));

            
            #line default
            #line hidden
WriteLiteral("</div>\r\n<div");

WriteLiteral(" class=\"row-split\"");

WriteLiteral("></div>\r\n\r\n<div");

WriteLiteral(" class=\"col-title\"");

WriteLiteral(">");

            
            #line 49 "..\..\Views\Shared\_CustomerInfo.cshtml"
                  Write(Html.LabelFor(m => m.DeliveryAddress.Country));

            
            #line default
            #line hidden
WriteLiteral("</div>\r\n<div");

WriteLiteral(" class=\"col-control\"");

WriteLiteral(">");

            
            #line 50 "..\..\Views\Shared\_CustomerInfo.cshtml"
                    Write(Html.EditorFor(m => m.DeliveryAddress.Country));

            
            #line default
            #line hidden
WriteLiteral("</div>\r\n<div");

WriteLiteral(" class=\"row-split\"");

WriteLiteral("></div>\r\n\r\n<div");

WriteLiteral(" class=\"col-title\"");

WriteLiteral(">");

            
            #line 53 "..\..\Views\Shared\_CustomerInfo.cshtml"
                  Write(Html.LabelFor(m => m.DeliveryAddress.Region));

            
            #line default
            #line hidden
WriteLiteral("</div>\r\n<div");

WriteLiteral(" class=\"col-control\"");

WriteLiteral(">");

            
            #line 54 "..\..\Views\Shared\_CustomerInfo.cshtml"
                    Write(Html.EditorFor(m => m.DeliveryAddress.Region));

            
            #line default
            #line hidden
WriteLiteral("</div>\r\n<div");

WriteLiteral(" class=\"row-split\"");

WriteLiteral("></div>\r\n\r\n<div");

WriteLiteral(" class=\"col-title\"");

WriteLiteral(">");

            
            #line 57 "..\..\Views\Shared\_CustomerInfo.cshtml"
                  Write(Html.LabelFor(m => m.DeliveryAddress.City));

            
            #line default
            #line hidden
WriteLiteral("</div>\r\n<div");

WriteLiteral(" class=\"col-control\"");

WriteLiteral(">");

            
            #line 58 "..\..\Views\Shared\_CustomerInfo.cshtml"
                    Write(Html.EditorFor(m => m.DeliveryAddress.City));

            
            #line default
            #line hidden
WriteLiteral("</div>\r\n<div");

WriteLiteral(" class=\"row-split\"");

WriteLiteral("></div>\r\n\r\n<div");

WriteLiteral(" class=\"col-title\"");

WriteLiteral(">");

            
            #line 61 "..\..\Views\Shared\_CustomerInfo.cshtml"
                  Write(Html.LabelFor(m => m.DeliveryAddress.Street));

            
            #line default
            #line hidden
WriteLiteral("</div>\r\n<div");

WriteLiteral(" class=\"col-control\"");

WriteLiteral(">");

            
            #line 62 "..\..\Views\Shared\_CustomerInfo.cshtml"
                    Write(Html.EditorFor(m => m.DeliveryAddress.Street));

            
            #line default
            #line hidden
WriteLiteral("</div>\r\n<div");

WriteLiteral(" class=\"row-split\"");

WriteLiteral("></div>\r\n\r\n<div");

WriteLiteral(" class=\"col-title\"");

WriteLiteral(">");

            
            #line 65 "..\..\Views\Shared\_CustomerInfo.cshtml"
                  Write(Html.LabelFor(m => m.DeliveryAddress.Building));

            
            #line default
            #line hidden
WriteLiteral("</div>\r\n<div");

WriteLiteral(" class=\"col-control\"");

WriteLiteral(">");

            
            #line 66 "..\..\Views\Shared\_CustomerInfo.cshtml"
                    Write(Html.EditorFor(m => m.DeliveryAddress.Building));

            
            #line default
            #line hidden
WriteLiteral("</div>\r\n<div");

WriteLiteral(" class=\"row-split\"");

WriteLiteral("></div>\r\n\r\n<div");

WriteLiteral(" class=\"col-title\"");

WriteLiteral(">");

            
            #line 69 "..\..\Views\Shared\_CustomerInfo.cshtml"
                  Write(Html.LabelFor(m => m.DeliveryAddress.Room));

            
            #line default
            #line hidden
WriteLiteral("</div>\r\n<div");

WriteLiteral(" class=\"col-control\"");

WriteLiteral(">");

            
            #line 70 "..\..\Views\Shared\_CustomerInfo.cshtml"
                    Write(Html.EditorFor(m => m.DeliveryAddress.Room));

            
            #line default
            #line hidden
WriteLiteral("</div>\r\n<div");

WriteLiteral(" class=\"row-split\"");

WriteLiteral("></div>\r\n\r\n<div");

WriteLiteral(" class=\"col-title\"");

WriteLiteral(">");

            
            #line 73 "..\..\Views\Shared\_CustomerInfo.cshtml"
                  Write(Html.LabelFor(m => m.DeliveryAddress.Special));

            
            #line default
            #line hidden
WriteLiteral("</div>\r\n<div");

WriteLiteral(" class=\"col-control\"");

WriteLiteral(">");

            
            #line 74 "..\..\Views\Shared\_CustomerInfo.cshtml"
                    Write(Html.EditorFor(m => m.DeliveryAddress.Special));

            
            #line default
            #line hidden
WriteLiteral("</div>\r\n<div");

WriteLiteral(" class=\"row-split\"");

WriteLiteral("></div>\r\n\r\n<div");

WriteLiteral(" class=\"col-title\"");

WriteLiteral(">");

            
            #line 77 "..\..\Views\Shared\_CustomerInfo.cshtml"
                  Write(Html.LabelFor(m => m.DeliveryAddress.Notes));

            
            #line default
            #line hidden
WriteLiteral("</div>\r\n<div");

WriteLiteral(" class=\"col-control\"");

WriteLiteral(">");

            
            #line 78 "..\..\Views\Shared\_CustomerInfo.cshtml"
                    Write(Html.EditorFor(m => m.DeliveryAddress.Notes));

            
            #line default
            #line hidden
WriteLiteral("</div>\r\n<div");

WriteLiteral(" class=\"row-split\"");

WriteLiteral("></div>\r\n</div>");

        }
    }
}
#pragma warning restore 1591
