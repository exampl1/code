﻿using C2.DataModel.StoreModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace C2.WebApp.Store.Models
{
    public class OrderViewModel
    {
        public OrderViewParameters Parameters
        {
            get;
            set;
        }

        public List<Order> Orders
        {
            get;
            set;
        }
    }
}