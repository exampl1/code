﻿using C2.DataModel.StoreModel;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataMapping.Store
{
    public class CustomerInfoMap : EntityTypeConfiguration<CustomerInfo>
    {
        public CustomerInfoMap()
        {
            ToTable("CustomerInfo");

            HasKey(t => t.UserName);

            HasRequired(t => t.DeliveryAddress);
        }
    }
}
