﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2.DataModel.StoreModel;

namespace C2.DataMapping.Store
{
    public class ProductMap : EntityTypeConfiguration<Product>
    {
        public ProductMap()
        {
            ToTable("Product");

            Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            HasMany(t => t.Units)
                .WithMany()
                .Map(m =>
                {
                    m.ToTable("ProductUnit");
                    m.MapLeftKey("UnitId");
                    m.MapRightKey("ProductId");
                });

            HasMany(t => t.Properties)
                .WithMany()
                .Map(m =>
                {
                    m.ToTable("ProductPropertyValue");
                    m.MapLeftKey("ProductId");
                    m.MapRightKey("PropertyValueId");
                });

            HasMany(t => t.ProductGroups)
                .WithMany()
                .Map(m =>
                {
                    m.ToTable("ProductInGroup");
                    m.MapLeftKey("ProductId");
                    m.MapRightKey("ProductGroupId");
                });


            HasMany(t => t.MediaCollection)
                .WithRequired()
                .HasForeignKey(m => m.CollectionHostId);

            HasMany(t => t.Comments)
                .WithOptional()
                .Map(m =>
                {
                    m.MapKey("EntityId");
                });
        }
    }
}
