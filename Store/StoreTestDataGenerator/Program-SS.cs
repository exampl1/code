﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2.DataModel;
using C2.DataModel.StoreModel;
using C2.DataMapping.Store;
using C2.DAL;
using C2.Globalization;
using System.Globalization;
using System.Data.Entity;
using System.IO;
using System.Text.RegularExpressions;
using System.Drawing;
using CsvHelper;

namespace StoreTestDataGenerator
{
    class ProgramSibsale
    {
        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                Cfg.VendorCode = args[0];
                if (args.Length > 1)
                {
                    Cfg.ImportFile = args[1];
                }
            }
            else
            {
                new Auto();
                Console.ReadKey();
                return;
            }

            ResourceManager.Init(new FakeResourceProvider(), new List<CultureInfo> { CultureInfo.GetCultureInfo("ru-RU") }, CultureInfo.GetCultureInfo("ru-RU"));

            CsvReader reader = new CsvReader(new StreamReader(File.OpenRead(Cfg.ImportFile), Encoding.Default));
            reader.Configuration.Delimiter = ';';
            reader.Configuration.FieldCount = 10;
            reader.Configuration.Quote = '"';

            List<Auto> records = new List<Auto>();

            while (reader.Read())
            {
                try
                {
                    records.Add(new Auto {
                        VendorCode = reader.GetField(0).Trim(),
                        PropertySet = reader.GetField(1).Trim(),
                        Qty = reader.GetField<int>(2),
                        Price = reader.GetField(3).Trim(),
                        Title = reader.GetField(4).Trim(),
                        Category = reader.GetField(5).Trim(),
                        Collection = reader.GetField(6).Trim(),
                        Description = reader.GetField(7).Trim(),
                    });

                    Console.WriteLine("item found: {0}", reader.GetField(0));
                }
                catch (Exception ex)
                {
                    Console.WriteLine("error: {0}", ex.Message);
                }
            }

            var allfiles = Directory.GetFiles(Cfg.LookupPath);

            //Хэш с файлами вида vendorcode => {file1, file2,..}
            Dictionary<string, List<string>> files = new Dictionary<string, List<string>>();

            foreach (var file in allfiles)
            {
                Console.WriteLine("{1}file found: {0}", file, Environment.NewLine);

                var nameonly = Path.GetFileNameWithoutExtension(file);
                var key = Dress.GetKey(nameonly);

                var keys = records.Where(r => key.Contains(r.Key)).Select(x => x.Key).ToList();

                foreach (var k in keys)
                {
                    Console.WriteLine("key found: {0}", k);
                    if (!files.ContainsKey(k))
                    {
                        files.Add(k, new List<string>(new string[] { file }));
                    }
                    else
                    {
                        if (!files[k].Contains(file))
                        {
                            files[k].Add(file);
                        }
                    }
                }

            }


            Console.WriteLine("Press any key to go on...");
            Console.ReadKey();

            using (var dbContext = new StoreContext("DefaultConnection"))
            {
                var prods = dbContext.Products
                    .Include(p => p.Localization)
                    .Include(p => p.Properties)
                    .Include(p => p.Offers)
                    .Include(p => p.Offers.Select(x=>x.Properties));
                
                foreach (var rec in records)
                {
                    bool hasFile = false;
                    var key = rec.Key;
                    foreach (var f in files.Keys.OrderBy(x => x))
                    {
                        if (f.Contains(key))
                        {
                            //файл есть.
                            hasFile = true;
                            key = f;
                            break;
                        }
                    }

                    if (!hasFile)
                    {
                        Console.WriteLine("item {0} has no file {1}", rec.VendorCode, key);
                        continue;
                    }

                    Console.WriteLine("working: {0}, key {1}", rec.VendorCode, key);

                    var product = prods.Where(p => p.VendorCode == rec.VendorCode).FirstOrDefault();
                    if (product != null)
                    {
                        //продукт есть. обновим данные.
                        dbContext.MediaCollectionEntries.Where(e => e.CollectionHostId == product.Id).ToList().ForEach(x => dbContext.MediaCollectionEntries.Remove(x));
                        dbContext.SaveChanges();

                        rec.Update(dbContext, product, (files[key] as List<string>).ToArray());
                    }
                    else
                    {
                        rec.Save(dbContext, (files[key] as List<string>).ToArray());
                    }

                    dbContext.SaveChanges();

                }

            }
        }

    }
}
