﻿using C2.DataMapping.Store;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Util;
using System.Data.Entity;

namespace StoreTestDataGenerator
{
    class GrabberPrima : GrabberBase
    {
        static GrabberPrima()
        {
            Cookie _cookie = new Cookie("ci_session", Properties.Settings.Default.cookie, "/", ".primalinea.ru"); 
            
            Http.Cookie = new CookieContainer();
            Http.Cookie.Add(_cookie);

            _start_ulr = @"http://primalinea.ru/catalog";
            _base = @"http://primalinea.ru";
            _imgPattern = @"<a href=[""'](http://primalinea.ru/images/.*?)[""']";
            _collection = @"D/";
            _pageItemPattern = "{0}/category/{1}/all/0/";
            _startCatalogString = "class=\"catalog-items";
            _catalogItemPattern = @"href=[""'].*?(/catalog/item/(.*?))[""']";

            xBind.Add("Category", @"<div class=[""']article-content[""']>\s*<h1>(.*?)</h1>");
            xBind.Add("Price", @"<h2>(.*?)RUB</h2>");
            xBind.Add("VendorCode", @"Артикул:</span>\s*(.*?)\s*</div>");
            xBind.Add("Title", @"<h2 style=[""']margin-bottom: 0px;[""']>(.*?)</h2>");
            xBind.Add("SizeSet", @"Размеры:</span>\s*(.*?)\s*</div>");
            xBind.Add("PropertySet", @"Состав:</span>\s*(.*?)\s*</div>");
            //xBind.Add("ColorSet", "Цвет: (.*?)");
            xBind.Add("Description", @"item-description-text.*?>(.*?)</div>");
        }

        public void Grab()
        {
            using (var dbContext = new StoreContext("DefaultConnection"))
            {
                foreach (int p in new int[] { 19, 22, 20, 13, 5, 29, 28, 1, 21, 6, 30, 24, 23 })
                {
                    FindItemPages(p).ToList().ForEach(x => SaveFromUrl(dbContext, x));
                }
            }
        }

    }
}
