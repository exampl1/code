﻿using C2.DataMapping.Store;
using C2.DataModel.StoreModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using C2.DataModel;
using C2.DAL;
using System.Configuration;

namespace StoreTestDataGenerator
{
    public class DataManager
    {
        static IdGenerator idGenerator;
        public static Subject Subject;
        public static Classifier Root;

        static DataManager()
        {
            using (var dbContext = new StoreContext(Cfg.Connection))
            {
                Subject = dbContext.Subjects.Where(s => s.Code == Cfg.VendorCode).Single();
                Root = dbContext.Classifiers.Where(c => c.Code == ProductGroup.DefaultClassifierCode).Single();
            }

            idGenerator = IdGenerator.CreateIdentityGenerator(ConfigurationManager.ConnectionStrings[Cfg.Connection].ConnectionString, "dbo.UidTable");
        }

        public static string GetProductFolderName(string vendorCode)
        {
            foreach (char ch in Path.GetInvalidPathChars())
            {
                vendorCode = vendorCode.Replace(ch, '_');
            }

            return vendorCode;
        }

        public static void SetupProductGroup(StoreContext dbContext, Product product, string title, string collection, string code = null)
        {
            var ProductClassisierQuery = from cn in dbContext.ClassifierNodes
                                         join pg in dbContext.ProductGroups on cn.EntityId equals pg.Id
                                         select new { pg, cn };

            var ProductClassisierSubQuery = ProductClassisierQuery;
            
            if (!string.IsNullOrWhiteSpace(collection) || Cfg.DoPutInCommonCat)
            {
                Console.WriteLine("\tCollection: {0}", collection);
                ProductClassisierSubQuery = from c in ProductClassisierQuery
                                            where c.cn.Path.StartsWith(collection)
                                            select c;

                var titles = title.Split(',');
                foreach (var tl in titles)
                {
                    var t = tl.Trim();
                    var prodGroup = ProductClassisierSubQuery.Where(g => g.pg.Localization.Any(l => l.Title == t)).Select(x => x.pg).FirstOrDefault();
                    prodGroup = SetupPGroup(dbContext, product, t, collection, prodGroup);
                }
            }

            if (code != null)
            {
                ProductClassisierSubQuery = from c in ProductClassisierQuery
                                            where c.cn.Code == code
                                            select c;


                var pnode = ProductClassisierSubQuery.Select(x => x.cn).FirstOrDefault();
                if (pnode == null)
                {
                    var supplier = dbContext.Subjects
                        .Include(s => s.Localization).Where(s => s.Code == code).Single();

                    var rgroup = AddProductGroup(dbContext, supplier.CurrentLocalization.Name, code);
                    pnode = NodeGroupLink(dbContext, rgroup, null);
                }

                SetupProductGroup(dbContext, product, title, pnode.Path);
            }

        }

        private static ProductGroup SetupPGroup(StoreContext dbContext, Product product, string title, string collection, ProductGroup prodGroup)
        {
            if (prodGroup == null && collection != null)
            {
                prodGroup = AddProductGroup(dbContext, title, null);

                var pnode = dbContext.ClassifierNodes.Where(n => n.Path == collection).FirstOrDefault();
                if (pnode != null)
                {
                    NodeGroupLink(dbContext, prodGroup, pnode);
                }
                else
                {
                    throw new ArgumentException("ClassifierNode not found");
                }
            }

            product.ProductGroups.Add(prodGroup);
            return prodGroup;
        }

        public static ClassifierNode NodeGroupLink(StoreContext dbContext, ProductGroup prodGroup, ClassifierNode pnode)
        {
            var node = dbContext.ClassifierNodes.Add(new ClassifierNode
            {
                EntityId = prodGroup.Id,
                Status = EntityStatus.Normal,
                Id = GetId(),
                Classifier = Root,
                Order = 0,
                Code = prodGroup.Code,
            });

            node.SetParent(pnode);

            dbContext.SaveChanges();
            return node;
        }

        public static ProductGroup AddProductGroup(StoreContext dbContext, string title, string code)
        {
            ProductGroup prodGroup;
            Console.WriteLine("new ProductGroup: {0}", title);
            prodGroup = dbContext.ProductGroups.Add(new ProductGroup
            {
                Id = GetId(),
                Status = EntityStatus.Normal,
                Code = code,
            });
            prodGroup.Localization.Add(new DictionaryItemLocalization
            {
                CultureCode = "ru-RU",
                Title = title,
                Description = title,
            });
            dbContext.SaveChanges();
            return prodGroup;
        }

        public static long GetId()
        {
            return idGenerator.GetId();
        }

    }
}
