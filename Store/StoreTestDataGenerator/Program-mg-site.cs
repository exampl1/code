﻿using C2.Globalization;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace StoreTestDataGenerator
{
    /// <summary>
    /// Загрузка с сайта http://magnolica-shop.ru
    /// ci_session:
    /// a%3A4%3A%7Bs%3A10%3A%22session_id%22%3Bs%3A32%3A%222cc75b7fc38e6a9b346d30da458298c9%22%3Bs%3A10%3A%22ip_address%22%3Bs%3A12%3A%22178.49.106.2%22%3Bs%3A10%3A%22user_agent%22%3Bs%3A50%3A%22Mozilla%2F5.0+%28Windows+NT+6.1%3B+WOW64%29+AppleWebKit%2F53%22%3Bs%3A13%3A%22last_activity%22%3Bi%3A1353304517%3B%7Dfb3bea2044b11ff2ba10b6df8392cc42
    /// </summary>

    class ProgramGrab
    {

        static void Main(string[] args)
        {
            Cfg.VendorCode = "primalinea";
            
            ResourceManager.Init(new FakeResourceProvider(), new List<CultureInfo> { CultureInfo.GetCultureInfo("ru-RU") }, CultureInfo.GetCultureInfo("ru-RU"));

            new GrabberPrima().Grab();
        }
    }



}
