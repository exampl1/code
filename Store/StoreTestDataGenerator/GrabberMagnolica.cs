﻿using C2.DataMapping.Store;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Util;
using System.Data.Entity;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.IO;

namespace StoreTestDataGenerator
{

    class GrabberMagnolica : GrabberBase
    {
        static GrabberMagnolica()
        {
            _base = @"http://http://trikotag-lv.ru";
            _start_ulr = _base + @"/catalog";

            _imgPattern = @"src=[""'](http://trikotag-lv.ru/sites/default/files/.*?)[""']";
            _collection = @"D/";

            _pageItemPattern = "not needed";
            _startCatalogString = "not needed";
            _catalogItemPattern = "not needed";

            xBind.Add("Category", @"<div class=[""']tr-content-product-title[""']>.*?<h2>.*?:\s+(.*?)\s+.*?</h2>");

            xBind.Add("Title", @"<div class=[""']tr-content-product-title[""']>.*?<h2>.*?:\s+(.*?)\s+.*?</h2>");
            xBind.Add("Price", @"<price>(.*?)</price>");
            xBind.Add("VendorCode", @"<div class=[""']tr-content-product-title[""']>.*?<h2>.*?:\s+.*?\s+(.*?)</h2>");

            xBind.Add("ColorSet", @"<div class=[""']tr-content-product-color-body[""']>(.*?)</div>");
            xBind.Add("SizeSet", @"<div class=[""']tr-content-product-size-change-border[""']>(.*?)</div>");

            //xBind.Add("PropertySet", @"Состав:</span>\s*(.*?)\s*</div>");


            xBind.Add("Description", @"<div class=[""']col-11 pre-01 post-01[""']>(.*?)</div>");

            Cfg.ColorSetPattern = @">([^>]+?)</span></span>";
            Cfg.SizeSetPattern = @">([^>]+?)</a>";
            Cfg.CurrencyCode = "USD";
            Cfg.PriceCorrection = 0.2m;
            Cfg.StoreRoot = "c:/inetpub/WebApp.Store.Sport";
            Cfg.VendorCode = "tnf";
            Cfg.Connection = "TNFConnection";

            ImgEvaluator = imgReplacer;
        }

        public static string imgReplacer(string input, IItem item)
        {
            //http://images.thenorthface.com/is/image/TheNorthFace/370x370/men-39-s-apex-bionic-jacket-AMVY_WY1_hero.jpg&amp;op_usm=1,1,8,0&amp;resmode=Sharp2
            var imgstart = input.ToLower().IndexOf(item.Key);
            var pathstart = input.IndexOf("TheNorthFace/") + 13;
            var src = input.Substring(0, pathstart) + input.Substring(imgstart);
            src = src.Replace(".jpg&amp;", "?") + "&wid=600&hei=900&fit=fit,1";
            Console.WriteLine(src);
            return src;
        }

        public void Grab()
        {
            var http = new Http();
            int ppp = 36;
            using (var dbContext = new StoreContext(Cfg.Connection))
            {
                foreach (int p in new int[] { 11703, 11704, 11719, 11720, 11721, 11702 })
                {
                    var data = http.GetPage(
                        "http://www.thenorthface.com/webapp/wcs/stores/servlet/TNFSearchResultJSON?" +
                        "cat=" + p + "&storeId=207&langId=-1&catalogId=10201&brandSCID=10051&gearSCID=10201&page=1&view=M&sort=default&products_per_page=" + ppp
                        );

                    var products = JSON.Deserialize(data);

                    products.products.Select(pr => pr.URL_productDetails).ToList().ForEach(x => SaveFromUrl(dbContext, x));

                    if (products.totalPages > 1)
                    {
                        for (int page = 2; page <= products.totalPages; page++)
                        {

                            data = http.GetPage(
                                "http://www.thenorthface.com/webapp/wcs/stores/servlet/TNFSearchResultJSON?" +
                                "cat=" + p + "&storeId=207&langId=-1&catalogId=10201&brandSCID=10051&gearSCID=10201&page=" + page + "&view=M&sort=default&products_per_page=" + ppp
                                );

                            var moreProducts = JSON.Deserialize(data);

                            moreProducts.products.Select(pr => pr.URL_productDetails).ToList().ForEach(x => SaveFromUrl(dbContext, x));
                        }

                    }
                }
            }
        }

    }
}
