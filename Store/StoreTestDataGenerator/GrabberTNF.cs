﻿using C2.DataMapping.Store;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Util;
using System.Data.Entity;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.IO;

namespace StoreTestDataGenerator
{
    [DataContract]
    class TNF
    {
        [DataMember]
        public TNFProduct[] products { get; set; }

        [DataMember]
        public int totalItems { get; set; }

        [DataMember]
        public int totalPages { get; set; }
        
    }


    [DataContract]
    class TNFProduct
    {
        [DataMember]
        public string URL_productDetails { get; set; }
    }

    class JSON
    {
        private static readonly DataContractJsonSerializer _jsonSerializer = new DataContractJsonSerializer(typeof(TNF));
        public static TNF Deserialize(string json)
        {
            if (String.IsNullOrEmpty(json))
            {
                throw new ArgumentNullException("json");
            }
            var res = Deserialize(new MemoryStream(Encoding.UTF8.GetBytes(json)));

            return res;
        }

        public static TNF Deserialize(Stream jsonStream)
        {
            if (jsonStream == null)
            {
                throw new ArgumentNullException("jsonStream");
            }

            return (TNF)_jsonSerializer.ReadObject(jsonStream);
        }
    }

    class GrabberTNF : GrabberBase
    {
        static GrabberTNF()
        {
            _base = @"http://www.thenorthface.com";
            _start_ulr = _base + @"/catalog/sc-gear";

            _imgPattern = @"src=[""'](http://images.thenorthface.com/is/image/TheNorthFace/370x370/.*?)[""']";
            _collection = @"CB/";
            
            _pageItemPattern = "not needed";
            _startCatalogString = "not needed";
            _catalogItemPattern = "not needed";

            xBind.Add("Category", @"<div class=[""']crumbs[""']>.*?back\(1\);[""']>(.*?)</a>.*?class");

            xBind.Add("Title", @"<h2 class=[""']heading[""']>(.*?)</h2>");
            xBind.Add("Price", @"class=[""']productPrice[""']>.*?\$(.*?)\.");
            xBind.Add("VendorCode", @"Style</span>(.*?)</li>");
            xBind.Add("ColorSet", @"class=[""']productColors[""']>(.*?)</li>");
            xBind.Add("SizeSet", @"class=[""']productSize[""']>(.*?)</li>");

            //xBind.Add("PropertySet", @"Состав:</span>\s*(.*?)\s*</div>");


            xBind.Add("Description", @"<div class=[""']col-11 pre-01 post-01[""']>(.*?)</div>");

            Cfg.ColorSetPattern = @">([^>]+?)</span></span>";
            Cfg.SizeSetPattern = @">([^>]+?)</a>";
            Cfg.CurrencyCode = "USD";
            Cfg.PriceCorrection = 0.2m;
            Cfg.StoreRoot = "c:/inetpub/WebApp.Store.Sport";
            Cfg.VendorCode = "tnf";
            Cfg.Connection = "TNFConnection";

            ImgEvaluator = imgReplacer;
        }

        public static string imgReplacer(string input, IItem item)
        {
            //http://images.thenorthface.com/is/image/TheNorthFace/370x370/men-39-s-apex-bionic-jacket-AMVY_WY1_hero.jpg&amp;op_usm=1,1,8,0&amp;resmode=Sharp2
            var imgstart = input.ToLower().IndexOf(item.Key);
            var pathstart = input.IndexOf("TheNorthFace/") + 13;
            var src = input.Substring(0, pathstart) + input.Substring(imgstart);
            src = src.Replace(".jpg&amp;", "?") + "&wid=600&hei=900&fit=fit,1";
            Console.WriteLine(src);
            return src;
        }

        public void Grab()
        {
            var http = new Http();
            int ppp = 36;
            using (var dbContext = new StoreContext(Cfg.Connection))
            {
                foreach (int p in new int[] { 11703, 11704, 11719, 11720, 11721, 11702 })
                {
                    var data = http.GetPage(
                        "http://www.thenorthface.com/webapp/wcs/stores/servlet/TNFSearchResultJSON?"+
                        "cat=" + p + "&storeId=207&langId=-1&catalogId=10201&brandSCID=10051&gearSCID=10201&page=1&view=M&sort=default&products_per_page=" + ppp
                        );

                    var products = JSON.Deserialize(data);

                    products.products.Select(pr => pr.URL_productDetails).ToList().ForEach(x => SaveFromUrl(dbContext, x));

                    if (products.totalPages > 1)
                    {
                        for (int page = 2; page <= products.totalPages; page++)
                        {

                            data = http.GetPage(
                                "http://www.thenorthface.com/webapp/wcs/stores/servlet/TNFSearchResultJSON?"+
                                "cat=" + p + "&storeId=207&langId=-1&catalogId=10201&brandSCID=10051&gearSCID=10201&page="+ page +"&view=M&sort=default&products_per_page=" + ppp
                                );

                            var moreProducts = JSON.Deserialize(data);

                            moreProducts.products.Select(pr => pr.URL_productDetails).ToList().ForEach(x => SaveFromUrl(dbContext, x));
                        }

                    }
                }
            }
        }

    }
}
