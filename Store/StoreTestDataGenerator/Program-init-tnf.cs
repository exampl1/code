﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2.DataModel;
using C2.DataModel.StoreModel;
using C2.DataMapping.Store;
using C2.DAL;
using C2.Globalization;
using System.Globalization;
using System.Data.Entity;
using System.IO;
using System.Text.RegularExpressions;
using System.Drawing;
using CsvHelper;

namespace StoreTestDataGenerator
{
    class ProgramInitTNF
    {
        static IdGenerator idGenerator;
        static ProgramInitTNF()
        {
            idGenerator = IdGenerator.CreateIdentityGenerator(ConfigurationManager.ConnectionStrings["TNFConnection"].ConnectionString, "dbo.UidTable");
        }

        static void Main(string[] args)
        {

            string code = "BBSW";
            ResourceManager.Init(new FakeResourceProvider(), new List<CultureInfo> { CultureInfo.GetCultureInfo("ru-RU") }, CultureInfo.GetCultureInfo("ru-RU"));

            using (var dbContext = new StoreContext("TNFConnection"))
            {
                var classifier = dbContext.Classifiers.Where(c => c.Code == "ProductGroup.Default").FirstOrDefault();
                if (classifier == null)
                {
                    classifier = dbContext.Classifiers.Add(new Classifier
                    {
                        Code = ProductGroup.DefaultClassifierCode,
                        Id = GetId(),
                        Status = EntityStatus.Normal,
                        ClassifierType = 1,
                    });

                    classifier.Localization.Add(new ClassifierLocalization
                    {
                        CultureCode="ru-RU",
                        Description = "Классификатор товарных групп по умолчанию",
                        Title = "Группы товаров",
                    });
                }


                // настройки сайта
                var sub = dbContext.Subjects.Where(s => s.Code == code).FirstOrDefault();
                if (sub == null)
                {
                    
                    sub = dbContext.Subjects.Add(new Subject
                    {
                        Id = GetId(),
                        Code = code,
                        Status = EntityStatus.Normal,
                        SubjectType = SubjectType.Organization,
                        Email = "manager@bbsw.ru",
                    });
                    
                    sub.Localization.Add(new SubjectLocalization
                    {
                        CultureCode="ru-RU",
                        Description = "Спорт со вкусом",
                        FullName = "Спорт со вкусом",
                        ShortName = "Спорт со вкусом",
                        Name = "Спорт со вкусом",
                    });
                }

                // основные
                var suplier_data = new Supplier
                {
                    Logo = "BBSW.ru <small>Спорт со вкусом</small>",
                    Domain = "bbsw.ru",
                    PhoneBlock = "<span>8 (800) 555-13-08 звонок бесплатный</span><span>8 (495) 215-08-81 Москва</span><span>8 (383) 286-27-55 Новосибирск</span>",
                    FirstFooterBlock = "<li><a href='/'>Интернет-магазин</a></li><li><a href='/home/get/business/'>Оптовым клиентам</a></li><li><a href='/home/get/order/'>Сделать заказ</a></li><li><a href='/home/get/preorder/'>Заказ по предоплате</a></li><li><a href='/home/get/sizetable/'>Таблица размеров</a></li>",
                    SecondFooterBlock = "<li><a href='/product/'>Все товары</a></li><li><a href='/comment/'>Отзывы</a></li></li>",
                    ThirdFooterBlock = "",
                    LeftFooterBlock = @"<p><a href='http://clck.yandex.ru/redir/dtype=stred/pid=47/cid=2508/*http://grade.market.yandex.ru/?id=145446&action=link'><img src='http://clck.yandex.ru/redir/dtype=stred/pid=47/cid=2505/*http://grade.market.yandex.ru/?id=145446&action=image&size=0' border='0' width='88' height='31' alt='Читайте отзывы покупателей и оценивайте качество магазина на Яндекс.Маркете' /></a></p>",
                    YaId = "18983335",
                };

                var suplier = dbContext.Suppliers.Where(s => s.Domain == suplier_data.Domain).FirstOrDefault();
                if (suplier == null)
                {
                    suplier_data.SubjectId = sub.Id;
                    dbContext.Suppliers.Add(suplier_data);
                }
                else
                {
                    suplier.Banner = suplier_data.Banner;
                    suplier.Domain = suplier_data.Domain;
                    suplier.Logo = suplier_data.Logo;
                    suplier.PhoneBlock = suplier_data.PhoneBlock;
                    suplier.FirstFooterBlock = suplier_data.FirstFooterBlock;
                    suplier.SecondFooterBlock = suplier_data.SecondFooterBlock;
                    suplier.ThirdFooterBlock = suplier_data.ThirdFooterBlock;
                    suplier.PartnerCode = suplier_data.PartnerCode;
                    suplier.LeftFooterBlock = suplier_data.LeftFooterBlock;
                    suplier.YaId = suplier_data.YaId;
                }

                // рамазанов
                var ramazanovcode = "nsksport";
                sub = dbContext.Subjects.Where(s => s.Code == ramazanovcode).FirstOrDefault();
                if (sub == null)
                {

                    sub = dbContext.Subjects.Add(new Subject
                    {
                        Id = GetId(),
                        Code = ramazanovcode,
                        Status = EntityStatus.Normal,
                        SubjectType = SubjectType.Person,
                        Email = "manager@bbsw.ru",
                    });

                    sub.Localization.Add(new SubjectLocalization
                    {
                        CultureCode = "ru-RU",
                        Description = "Спортивная одежда в Новосибирске",
                        FullName = "Спортивная одежда в Новосибирске",
                        ShortName = "Спортивная одежда",
                        Name = "Спортивная одежда в Новосибирске",
                    });
                }

                suplier_data.Logo = "Одежда для сильных";
                suplier_data.Domain = "xn--j1afedgcbl.xn--p1ai";
                suplier_data.PartnerCode = 26702;
                suplier_data.YaId = "20547886";

                suplier = dbContext.Suppliers.Where(s => s.Domain == suplier_data.Domain).FirstOrDefault();
                if (suplier == null)
                {
                    suplier_data.SubjectId = sub.Id;
                    dbContext.Suppliers.Add(suplier_data);
                }
                else
                {
                    suplier.Banner = suplier_data.Banner;
                    suplier.Domain = suplier_data.Domain;
                    suplier.Logo = suplier_data.Logo;
                    suplier.PhoneBlock = suplier_data.PhoneBlock;
                    suplier.FirstFooterBlock = suplier_data.FirstFooterBlock;
                    suplier.SecondFooterBlock = suplier_data.SecondFooterBlock;
                    suplier.ThirdFooterBlock = suplier_data.ThirdFooterBlock;
                    suplier.PartnerCode = suplier_data.PartnerCode;
                }



                // Добавление торговых марок
                sub = dbContext.Subjects.Where(s => s.Code == "tnf").FirstOrDefault();
                if (sub == null)
                {

                    sub = dbContext.Subjects.Add(new Subject
                    {
                        Id = GetId(),
                        Code = "tnf",
                        Status = EntityStatus.Normal,
                        SubjectType = SubjectType.Organization,
                        Email = "manager@bbsw.ru",
                    });

                    sub.Localization.Add(new SubjectLocalization
                    {
                        CultureCode = "ru-RU",
                        Description = "The North Face",
                        FullName = "The North Face",
                        ShortName = "The North Face",
                        Name = "The North Face",
                    });
                }

                sub = dbContext.Subjects.Where(s => s.Code == "Daiyating").FirstOrDefault();
                if (sub == null)
                {

                    sub = dbContext.Subjects.Add(new Subject
                    {
                        Id = GetId(),
                        Code = "Daiyating",
                        Status = EntityStatus.Normal,
                        SubjectType = SubjectType.Organization,
                        Email = "manager@bbsw.ru",
                    });

                    sub.Localization.Add(new SubjectLocalization
                    {
                        CultureCode = "ru-RU",
                        Description = "Daiyating",
                        FullName = "Daiyating",
                        ShortName = "Daiyating",
                        Name = "Daiyating",
                    });
                }

                sub = dbContext.Subjects.Where(s => s.Code == "MYS").FirstOrDefault();
                if (sub == null)
                {

                    sub = dbContext.Subjects.Add(new Subject
                    {
                        Id = GetId(),
                        Code = "MYS",
                        Status = EntityStatus.Normal,
                        SubjectType = SubjectType.Organization,
                        Email = "manager@bbsw.ru",
                    });

                    sub.Localization.Add(new SubjectLocalization
                    {
                        CultureCode = "ru-RU",
                        Description = "MYS",
                        FullName = "MYS",
                        ShortName = "MYS",
                        Name = "MYS",
                    });
                }

                sub = dbContext.Subjects.Where(s => s.Code == "Deluxe").FirstOrDefault();
                if (sub == null)
                {

                    sub = dbContext.Subjects.Add(new Subject
                    {
                        Id = GetId(),
                        Code = "Deluxe",
                        Status = EntityStatus.Normal,
                        SubjectType = SubjectType.Organization,
                        Email = "manager@bbsw.ru",
                    });

                    sub.Localization.Add(new SubjectLocalization
                    {
                        CultureCode = "ru-RU",
                        Description = "Deluxe",
                        FullName = "Deluxe",
                        ShortName = "Deluxe",
                        Name = "Deluxe",
                    });
                }

                sub = dbContext.Subjects.Where(s => s.Code == "Hustler").FirstOrDefault();
                if (sub == null)
                {

                    sub = dbContext.Subjects.Add(new Subject
                    {
                        Id = GetId(),
                        Code = "Hustler",
                        Status = EntityStatus.Normal,
                        SubjectType = SubjectType.Organization,
                        Email = "manager@bbsw.ru",
                    });

                    sub.Localization.Add(new SubjectLocalization
                    {
                        CultureCode = "ru-RU",
                        Description = "Hustler",
                        FullName = "Hustler",
                        ShortName = "Hustler",
                        Name = "Hustler",
                    });
                }

                sub = dbContext.Subjects.Where(s => s.Code == "Electric").FirstOrDefault();
                if (sub == null)
                {

                    sub = dbContext.Subjects.Add(new Subject
                    {
                        Id = GetId(),
                        Code = "Electric",
                        Status = EntityStatus.Normal,
                        SubjectType = SubjectType.Organization,
                        Email = "manager@bbsw.ru",
                    });

                    sub.Localization.Add(new SubjectLocalization
                    {
                        CultureCode = "ru-RU",
                        Description = "Electric",
                        FullName = "Electric",
                        ShortName = "Electric",
                        Name = "Electric",
                    });
                }
                sub = dbContext.Subjects.Where(s => s.Code == "Burberry").FirstOrDefault();
                if (sub == null)
                {

                    sub = dbContext.Subjects.Add(new Subject
                    {
                        Id = GetId(),
                        Code = "Burberry",
                        Status = EntityStatus.Normal,
                        SubjectType = SubjectType.Organization,
                        Email = "manager@bbsw.ru",
                    });

                    sub.Localization.Add(new SubjectLocalization
                    {
                        CultureCode = "ru-RU",
                        Description = "Burberry",
                        FullName = "Burberry",
                        ShortName = "Burberry",
                        Name = "Burberry",
                    });
                }

                sub = dbContext.Subjects.Where(s => s.Code == "okiss").FirstOrDefault();
                if (sub == null)
                {

                    sub = dbContext.Subjects.Add(new Subject
                    {
                        Id = GetId(),
                        Code = "okiss",
                        Status = EntityStatus.Normal,
                        SubjectType = SubjectType.Organization,
                        Email = "manager@bbsw.ru",
                    });

                    sub.Localization.Add(new SubjectLocalization
                    {
                        CultureCode = "ru-RU",
                        Description = "Купальники O-Kiss",
                        FullName = "Купальники O-Kiss",
                        ShortName = "O-Kiss",
                        Name = "O-Kiss",
                    });
                }

                sub = dbContext.Subjects.Where(s => s.Code == "Bogner").FirstOrDefault();
                if (sub == null)
                {

                    sub = dbContext.Subjects.Add(new Subject
                    {
                        Id = GetId(),
                        Code = "Bogner",
                        Status = EntityStatus.Normal,
                        SubjectType = SubjectType.Organization,
                        Email = "manager@bbsw.ru",
                    });

                    sub.Localization.Add(new SubjectLocalization
                    {
                        CultureCode = "ru-RU",
                        Description = "Bogner",
                        FullName = "Bogner",
                        ShortName = "Bogner",
                        Name = "Bogner",
                    });
                }

                sub = dbContext.Subjects.Where(s => s.Code == "CARRERA").FirstOrDefault();
                if (sub == null)
                {

                    sub = dbContext.Subjects.Add(new Subject
                    {
                        Id = GetId(),
                        Code = "CARRERA",
                        Status = EntityStatus.Normal,
                        SubjectType = SubjectType.Organization,
                        Email = "manager@bbsw.ru",
                    });

                    sub.Localization.Add(new SubjectLocalization
                    {
                        CultureCode = "ru-RU",
                        Description = "CARRERA",
                        FullName = "CARRERA",
                        ShortName = "CARRERA",
                        Name = "CARRERA",
                    });
                }

                sub = dbContext.Subjects.Where(s => s.Code == "Colmar").FirstOrDefault();
                if (sub == null)
                {

                    sub = dbContext.Subjects.Add(new Subject
                    {
                        Id = GetId(),
                        Code = "Colmar",
                        Status = EntityStatus.Normal,
                        SubjectType = SubjectType.Organization,
                        Email = "manager@bbsw.ru",
                    });

                    sub.Localization.Add(new SubjectLocalization
                    {
                        CultureCode = "ru-RU",
                        Description = "Colmar",
                        FullName = "Colmar",
                        ShortName = "Colmar",
                        Name = "Colmar",
                    });
                }

                sub = dbContext.Subjects.Where(s => s.Code == "Columbia").FirstOrDefault();
                if (sub == null)
                {

                    sub = dbContext.Subjects.Add(new Subject
                    {
                        Id = GetId(),
                        Code = "Columbia",
                        Status = EntityStatus.Normal,
                        SubjectType = SubjectType.Organization,
                        Email = "manager@bbsw.ru",
                    });

                    sub.Localization.Add(new SubjectLocalization
                    {
                        CultureCode = "ru-RU",
                        Description = "Columbia",
                        FullName = "Columbia",
                        ShortName = "Columbia",
                        Name = "Columbia",
                    });
                }                  
                
                sub = dbContext.Subjects.Where(s => s.Code == "HEAD").FirstOrDefault();
                if (sub == null)
                {

                    sub = dbContext.Subjects.Add(new Subject
                    {
                        Id = GetId(),
                        Code = "HEAD",
                        Status = EntityStatus.Normal,
                        SubjectType = SubjectType.Organization,
                        Email = "manager@bbsw.ru",
                    });

                    sub.Localization.Add(new SubjectLocalization
                    {
                        CultureCode = "ru-RU",
                        Description = "HEAD",
                        FullName = "HEAD",
                        ShortName = "HEAD",
                        Name = "HEAD",
                    });
                }

                sub = dbContext.Subjects.Where(s => s.Code == "JackWolfskin").FirstOrDefault();
                if (sub == null)
                {

                    sub = dbContext.Subjects.Add(new Subject
                    {
                        Id = GetId(),
                        Code = "JackWolfskin",
                        Status = EntityStatus.Normal,
                        SubjectType = SubjectType.Organization,
                        Email = "manager@bbsw.ru",
                    });

                    sub.Localization.Add(new SubjectLocalization
                    {
                        CultureCode = "ru-RU",
                        Description = "Jack Wolfskin",
                        FullName = "Jack Wolfskin",
                        ShortName = "Jack Wolfskin",
                        Name = "Jack Wolfskin",
                    });
                }

                sub = dbContext.Subjects.Where(s => s.Code == "NiceFace").FirstOrDefault();
                if (sub == null)
                {

                    sub = dbContext.Subjects.Add(new Subject
                    {
                        Id = GetId(),
                        Code = "NiceFace",
                        Status = EntityStatus.Normal,
                        SubjectType = SubjectType.Organization,
                        Email = "manager@bbsw.ru",
                    });

                    sub.Localization.Add(new SubjectLocalization
                    {
                        CultureCode = "ru-RU",
                        Description = "Nice Face",
                        FullName = "Nice Face",
                        ShortName = "Nice Face",
                        Name = "Nice Face",
                    });
                }

                sub = dbContext.Subjects.Where(s => s.Code == "OR").FirstOrDefault();
                if (sub == null)
                {

                    sub = dbContext.Subjects.Add(new Subject
                    {
                        Id = GetId(),
                        Code = "OR",
                        Status = EntityStatus.Normal,
                        SubjectType = SubjectType.Organization,
                        Email = "manager@bbsw.ru",
                    });

                    sub.Localization.Add(new SubjectLocalization
                    {
                        CultureCode = "ru-RU",
                        Description = "OR",
                        FullName = "OR",
                        ShortName = "OR",
                        Name = "OR",
                    });
                }

                sub = dbContext.Subjects.Where(s => s.Code == "Polisi").FirstOrDefault();
                if (sub == null)
                {

                    sub = dbContext.Subjects.Add(new Subject
                    {
                        Id = GetId(),
                        Code = "Polisi",
                        Status = EntityStatus.Normal,
                        SubjectType = SubjectType.Organization,
                        Email = "manager@bbsw.ru",
                    });

                    sub.Localization.Add(new SubjectLocalization
                    {
                        CultureCode = "ru-RU",
                        Description = "Polisi",
                        FullName = "Polisi",
                        ShortName = "Polisi",
                        Name = "Polisi",
                    });
                }

                sub = dbContext.Subjects.Where(s => s.Code == "QUIKSILVER").FirstOrDefault();
                if (sub == null)
                {

                    sub = dbContext.Subjects.Add(new Subject
                    {
                        Id = GetId(),
                        Code = "QUIKSILVER",
                        Status = EntityStatus.Normal,
                        SubjectType = SubjectType.Organization,
                        Email = "manager@bbsw.ru",
                    });

                    sub.Localization.Add(new SubjectLocalization
                    {
                        CultureCode = "ru-RU",
                        Description = "QUIKSILVER",
                        FullName = "QUIKSILVER",
                        ShortName = "QUIKSILVER",
                        Name = "QUIKSILVER",
                    });
                }

                sub = dbContext.Subjects.Where(s => s.Code == "SCOTT").FirstOrDefault();
                if (sub == null)
                {

                    sub = dbContext.Subjects.Add(new Subject
                    {
                        Id = GetId(),
                        Code = "SCOTT",
                        Status = EntityStatus.Normal,
                        SubjectType = SubjectType.Organization,
                        Email = "manager@bbsw.ru",
                    });

                    sub.Localization.Add(new SubjectLocalization
                    {
                        CultureCode = "ru-RU",
                        Description = "SCOTT",
                        FullName = "SCOTT",
                        ShortName = "SCOTT",
                        Name = "SCOTT",
                    });
                }

                sub = dbContext.Subjects.Where(s => s.Code == "ONEILL").FirstOrDefault();
                if (sub == null)
                {

                    sub = dbContext.Subjects.Add(new Subject
                    {
                        Id = GetId(),
                        Code = "ONEILL",
                        Status = EntityStatus.Normal,
                        SubjectType = SubjectType.Organization,
                        Email = "manager@bbsw.ru",
                    });

                    sub.Localization.Add(new SubjectLocalization
                    {
                        CultureCode = "ru-RU",
                        Description = "ONEILL",
                        FullName = "ONEILL",
                        ShortName = "ONEILL",
                        Name = "ONEILL",
                    });
                }

                sub = dbContext.Subjects.Where(s => s.Code == "MountainSpirit").FirstOrDefault();
                if (sub == null)
                {

                    sub = dbContext.Subjects.Add(new Subject
                    {
                        Id = GetId(),
                        Code = "MountainSpirit",
                        Status = EntityStatus.Normal,
                        SubjectType = SubjectType.Organization,
                        Email = "manager@bbsw.ru",
                    });

                    sub.Localization.Add(new SubjectLocalization
                    {
                        CultureCode = "ru-RU",
                        Description = "Mountain Spirit",
                        FullName = "Mountain Spirit",
                        ShortName = "Mountain Spirit",
                        Name = "Mountain Spirit",
                    });
                }

                sub = dbContext.Subjects.Where(s => s.Code == "Burton").FirstOrDefault();
                if (sub == null)
                {

                    sub = dbContext.Subjects.Add(new Subject
                    {
                        Id = GetId(),
                        Code = "Burton",
                        Status = EntityStatus.Normal,
                        SubjectType = SubjectType.Organization,
                        Email = "manager@bbsw.ru",
                    });

                    sub.Localization.Add(new SubjectLocalization
                    {
                        CultureCode = "ru-RU",
                        Description = "Burton",
                        FullName = "Burton",
                        ShortName = "Burton",
                        Name = "Burton",
                    });
                }

                sub = dbContext.Subjects.Where(s => s.Code == "Oakley").FirstOrDefault();
                if (sub == null)
                {

                    sub = dbContext.Subjects.Add(new Subject
                    {
                        Id = GetId(),
                        Code = "Oakley",
                        Status = EntityStatus.Normal,
                        SubjectType = SubjectType.Organization,
                        Email = "manager@bbsw.ru",
                    });

                    sub.Localization.Add(new SubjectLocalization
                    {
                        CultureCode = "ru-RU",
                        Description = "Oakley",
                        FullName = "Oakley",
                        ShortName = "Oakley",
                        Name = "Oakley",
                    });
                }

                sub = dbContext.Subjects.Where(s => s.Code == "Billabong").FirstOrDefault();
                if (sub == null)
                {

                    sub = dbContext.Subjects.Add(new Subject
                    {
                        Id = GetId(),
                        Code = "Billabong",
                        Status = EntityStatus.Normal,
                        SubjectType = SubjectType.Organization,
                        Email = "manager@bbsw.ru",
                    });

                    sub.Localization.Add(new SubjectLocalization
                    {
                        CultureCode = "ru-RU",
                        Description = "Billabong",
                        FullName = "Billabong",
                        ShortName = "Billabong",
                        Name = "Billabong",
                    });
                }

                sub = dbContext.Subjects.Where(s => s.Code == "Roxy").FirstOrDefault();
                if (sub == null)
                {

                    sub = dbContext.Subjects.Add(new Subject
                    {
                        Id = GetId(),
                        Code = "Roxy",
                        Status = EntityStatus.Normal,
                        SubjectType = SubjectType.Organization,
                        Email = "manager@bbsw.ru",
                    });

                    sub.Localization.Add(new SubjectLocalization
                    {
                        CultureCode = "ru-RU",
                        Description = "Roxy",
                        FullName = "Roxy",
                        ShortName = "Roxy",
                        Name = "Roxy",
                    });
                }

                sub = dbContext.Subjects.Where(s => s.Code == "Arcteryx").FirstOrDefault();
                if (sub == null)
                {

                    sub = dbContext.Subjects.Add(new Subject
                    {
                        Id = GetId(),
                        Code = "Arcteryx",
                        Status = EntityStatus.Normal,
                        SubjectType = SubjectType.Organization,
                        Email = "manager@bbsw.ru",
                    });

                    sub.Localization.Add(new SubjectLocalization
                    {
                        CultureCode = "ru-RU",
                        Description = "Arcteryx",
                        FullName = "Arcteryx",
                        ShortName = "Arcteryx",
                        Name = "Arcteryx",
                    });
                }

                sub = dbContext.Subjects.Where(s => s.Code == "Moncler").FirstOrDefault();
                if (sub == null)
                {

                    sub = dbContext.Subjects.Add(new Subject
                    {
                        Id = GetId(),
                        Code = "Moncler",
                        Status = EntityStatus.Normal,
                        SubjectType = SubjectType.Organization,
                        Email = "manager@bbsw.ru",
                    });

                    sub.Localization.Add(new SubjectLocalization
                    {
                        CultureCode = "ru-RU",
                        Description = "Moncler",
                        FullName = "Moncler",
                        ShortName = "Moncler",
                        Name = "Moncler",
                    });
                }

                var node = dbContext.ClassifierNodes.FirstOrDefault();
                if (node == null)
                {
                    var pg = dbContext.ProductGroups.Add(new ProductGroup
                    {
                        Id = GetId(),
                        Code = "root",
                        Status = EntityStatus.Normal,
                    });

                    pg.Localization.Add(new DictionaryItemLocalization
                    {
                        CultureCode = "ru-RU",
                        Title = "root",
                        Description = "root",
                    });

                    node = dbContext.ClassifierNodes.Add(new ClassifierNode
                    {
                        ClassifierId = classifier.Id,
                        Id = GetId(),
                        Code = "root",
                        EntityId = pg.Id,
                        Status = EntityStatus.Normal,
                    });

                    node.SetParent(null);
                }

                dbContext.SaveChanges();
                
                Console.WriteLine("done");
            }

            Console.ReadKey();
        }

        static long GetId()
        {
            return idGenerator.GetId();
        }
    }
}
