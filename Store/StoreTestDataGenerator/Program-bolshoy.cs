﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2.DataModel;
using C2.DataModel.StoreModel;
using C2.DataMapping.Store;
using C2.DAL;
using C2.Globalization;
using System.Globalization;
using System.Data.Entity;
using System.IO;
using System.Text.RegularExpressions;
using System.Drawing;
using CsvHelper;

namespace StoreTestDataGenerator
{
    class ProgramFireworks
    {
        static void Main(string[] args)
        {
            ResourceManager.Init(new FakeResourceProvider(), new List<CultureInfo> { CultureInfo.GetCultureInfo("ru-RU") }, CultureInfo.GetCultureInfo("ru-RU"));

            var lookuppath = string.Format("/media/bolshoy/");

            var allfiles = Directory.GetFiles(lookuppath);

            //Хэш с файлами вида vendorcode => {file1, file2,..}
            Hashtable files = new Hashtable();

            foreach (var file in allfiles)
            {
                var match = Regex.Match(Path.GetFileName(file), @"(.*?)\s*[+]*?\s*\.\w{3,4}", RegexOptions.IgnoreCase);
                if (match.Success)
                {
                    var key = Regex.Replace(match.Groups[1].Value, @"\W", "").ToLower();

                    Console.WriteLine("file found: {0}", file);

                    if (!files.Contains(key))
                    {
                        files.Add(key, new List<string>(new string[] { file }));
                    }
                    else
                    {
                        ((List<string>)files[key]).Add(file);
                    }
                }
            }

            CsvReader reader = new CsvReader(new StreamReader(File.OpenRead("salut.csv"), Encoding.Default));
            reader.Configuration.Delimiter = ';';
            reader.Configuration.FieldCount = 7;
            reader.Configuration.Quote = '"';

            List<Bolshoy> records = new List<Bolshoy>();
            while (reader.Read())
            {
                try
                {
                    records.Add(new Bolshoy
                    {
                        VendorCode = reader.GetField(0),
                        Title = reader.GetField(1),
                        
                        Category = reader.GetField(4),
                        Price = reader.GetField(5),
                        Description = reader.GetField(6),
                        Collection = reader.GetField(3),
                    });

                    Console.WriteLine("found: {0}", reader.GetField(0));
                }
                catch (Exception ex)
                {
                    Console.WriteLine("error: {0}", ex.Message);
                }
            }

            int i = 1;
            using (var dbContext = new StoreContext("DefaultConnection"))
            {
                var prods = dbContext.Products.Include(p => p.Localization);
                foreach (var rec in records)
                {
                    var key = Regex.Replace(rec.VendorCode, @"\W", "").ToLower();
                    bool hasFile = false;
                    foreach (DictionaryEntry f in files)
                    {
                        if (f.Key.ToString().Contains(key))
                        {
                            //файл есть.
                            hasFile = true;
                            key = f.Key.ToString();
                            break;
                        }
                    }

                    if (!hasFile)
                    {
                        continue;
                    }

                    var prod = prods.FirstOrDefault(p => p.VendorCode.StartsWith(rec.VendorCode));
                    Console.WriteLine("working: {0}", rec.VendorCode);

                    if (prod == null)
                    {
                        //Не нашлось продукта в базе, нужно добавлять новый
                        rec.Save(dbContext, (files[key] as List<string>).ToArray());
                        if (i++ % 30 == 0)
                        {
                            Console.WriteLine("saving..");
                            dbContext.SaveChanges();
                        }
                    }

                }

                Console.WriteLine("saving..");
                dbContext.SaveChanges();


            }
        }

    }
}
