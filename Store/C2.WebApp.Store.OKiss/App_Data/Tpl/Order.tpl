﻿<div style='font-family: Segoe UI, Segoe, Arial, sans-serif'>
  <p>Здравствуйте, {{CustomerInfo}}!</p>
  <p>Ваш заказ {{Number}} от {{Date}} на сайте <a href='http://bbsw.ru'>http://bbsw.ru</a> подтвержден менеджером и ожидает оплаты.</p>
  <h2>Состав заказа</h2>
  <table border='1' cellpadding='2' cellspacing='1'>
    <tr>
      <th>№</th>
      <th>Артикул</th>
      <th>Название товара</th>
      <th>Цена</th>
      <th>Количество</th>
      <th>Итого</th>
    </tr>
    {{foreach Items Items}}
  </table>
  <p>Итого: {{Total}} р.</p>
  <h2>Реквизиты для оплаты</h2>
  <div>
    <p>Общество с ограниченной ответственностью «ВебТек Групп» г. Новосибирск</p>

    <p>ИНН 5445014180</p>
    <p>КПП 544501001</p>
    <p>БИК 045004641</p>
    <p>р/с №40702810844050098162 в Сибирском банке Сбербанка России</p>
    <p>к/с 30101810500000000641</p>
  </div>
  {{include Footer}}
</div>