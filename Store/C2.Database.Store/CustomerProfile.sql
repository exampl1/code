﻿CREATE TABLE [dbo].[CustomerProfile]
(
	[CustomerId] [dbo].[EntityId] NOT NULL, 
    [Status] INT NOT NULL, 
    [CustomerGroupId] [dbo].[EntityId] NOT NULL,
    CONSTRAINT [PK_CustomerProfile] PRIMARY KEY ([CustomerId]), 
    CONSTRAINT [FK_CustomerProfile_ToSubject] FOREIGN KEY ([CustomerId]) REFERENCES [Subject]([Id]), 
    CONSTRAINT [FK_CustomerProfile_ToCustomerGroupDictionary] FOREIGN KEY ([CustomerGroupId]) REFERENCES [DictionaryItem]([Id])
)
