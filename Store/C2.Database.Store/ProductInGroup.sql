﻿CREATE TABLE [dbo].[ProductInGroup]
(
	[ProductGroupId] [dbo].[EntityId] NOT NULL , 
    [ProductId] [dbo].[EntityId] NOT NULL, 
    PRIMARY KEY ([ProductGroupId], [ProductId]), 
    CONSTRAINT [FK_ProductInGroup_ProductGroup] FOREIGN KEY ([ProductGroupId]) REFERENCES [DictionaryItem]([Id]),
    CONSTRAINT [FK_ProductInGroup_Product] FOREIGN KEY ([ProductId]) REFERENCES [Product]([Id])
)
