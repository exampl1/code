﻿CREATE TABLE [dbo].[CustomerInfo]
(
	[UserName] [dbo].[Code] NOT NULL,
    [ContactName] [dbo].[FullName] NOT NULL,
	[Email] NVARCHAR(50) NOT NULL,
	[Phone] NVARCHAR(50) NOT NULL,
    [Requisites] [dbo].[Description] NULL,
    [DeliveryAddressId] [dbo].[EntityId] NOT NULL,
    [Status] TINYINT NOT NULL DEFAULT 0 , 
    [Organization] NVARCHAR(1024) NULL, 
    [INN] NVARCHAR(12) NULL, 
    [KPP] NVARCHAR(9) NULL, 
    [OGRN] NVARCHAR(15) NULL, 
    CONSTRAINT [PK_CustomerInfo] PRIMARY KEY ([UserName]),
    CONSTRAINT [FK_CustomerInfo_ToAddress] FOREIGN KEY ([DeliveryAddressId]) REFERENCES [Address]([Id])
)
