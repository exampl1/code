﻿CREATE TABLE [dbo].[ProductUnit]
(
	[ProductId] [dbo].[EntityId] NOT NULL , 
    [UnitId] [dbo].[EntityId] NOT NULL, 
    CONSTRAINT [PK_ProductUnit] PRIMARY KEY ([ProductId], [UnitId])
)
