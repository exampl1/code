﻿CREATE TABLE [dbo].[OfferPropertyValue]
(
	[OfferId] [dbo].[EntityId] NOT NULL , 
    [PropertyValueId] [dbo].[EntityId] NOT NULL, 
    CONSTRAINT [PK_OfferPropertyValue] PRIMARY KEY ([OfferId], [PropertyValueId]),
	CONSTRAINT [FK_OfferPropertyValue_ToOffer] FOREIGN KEY ([OfferId]) REFERENCES [Offer]([Id]),
    CONSTRAINT [FK_OfferPropertyValue_ToPropertyValue] FOREIGN KEY ([PropertyValueId]) REFERENCES [PropertyValue]([Id])
)
