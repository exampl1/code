﻿CREATE TABLE [dbo].[Order]
(
	[Id] [dbo].[EntityId] NOT NULL,
    [Status] INT NOT NULL,
	[OrderStatus] INT NOT NULL,
    [CustomerId] [dbo].[Code] NOT NULL,
    [PayerId] [dbo].[Code] NOT NULL,
    [CreateDate] DATETIME NOT NULL,
	[PayDate] DATETIME,
	[CompleteDate] DATETIME,
	[CurrencyCode] [dbo].[CurrencyCode] NOT NULL,
    [SubTotal] MONEY NOT NULL,
    [DiscountTotal] MONEY NOT NULL,
    [TaxTotal] MONEY NOT NULL,
    [Total] MONEY NOT NULL,
	[TotalPayment] MONEY NOT NULL DEFAULT 0,
    [Delivery] NVARCHAR(MAX) NULL, 
    [AdminComment] NVARCHAR(MAX) NULL, 
    [UserComment] NVARCHAR(MAX) NULL, 
    [IsPreorder] BIT NOT NULL DEFAULT 0, 
    CONSTRAINT [PK_Order] PRIMARY KEY ([Id])
)
