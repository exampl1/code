﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2.Security;

namespace C2.Web.Mvc
{
    /// <summary>
    /// Repository of authorization providers.
    /// </summary>
    /// <remarks>
    /// MVC application must fill the Respository during starting process.
    /// </remarks>
    public class AuthorizationProviderRepository
    {
        /// <summary>
        /// Try get provider from repository.
        /// </summary>
        /// <param name="providerName"></param>
        /// <param name="provider"></param>
        /// <returns></returns>
        public static bool TryGetProvider(string providerName, out IAuthorizationProvider provider)
        {
            return _providers.TryGetValue(providerName, out provider);
        }

        /// <summary>
        /// Add provider into repository.
        /// </summary>
        /// <param name="providerName"></param>
        /// <param name="provider"></param>
        public static void AddProvider(string providerName, IAuthorizationProvider provider)
        {
            _providers.Add(providerName, provider);
        }

        static Dictionary<string, IAuthorizationProvider> _providers;

        /// <summary>
        /// Static constructor.
        /// </summary>
        static AuthorizationProviderRepository()
        {
            _providers = new Dictionary<string, IAuthorizationProvider>();
        }
    }
}
