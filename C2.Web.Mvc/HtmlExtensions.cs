﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Routing;

namespace System.Web.Mvc.Html
{
    public static class HtmlExtensions
    {
        public static IEnumerable<SelectListItem> ToSelectList(this Enum enumValue)
        {
            List<SelectListItem> _list = (from Enum e in Enum.GetValues(enumValue.GetType())
                   where e.ToString() != "None"
                   select new SelectListItem
                   {
                       Selected = e.Equals(enumValue),
                       Text = e.ToDescription(),
                       Value = e.ToString()
                   }).ToList();

            _list.Insert(0, new SelectListItem
            {
                Text = "Все",
                Value = "-1",
                Selected = enumValue.Equals(-1)
            });


            return _list;
        }

        public static string ToDescription(this Enum value)
        {
            var attr = value.GetType().GetField(value.ToString());
            if (attr != null)
            {
                var attributes = (DisplayAttribute[])attr.GetCustomAttributes(typeof(DisplayAttribute), false);
                return attributes.Length > 0 ? attributes[0].Name : value.ToString();
            }

            return value.ToString();
        }

        public static MvcHtmlString RadioButtonForSelectList<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            IEnumerable<SelectListItem> listOfValues)
        {
            var metaData = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            string fullName = ExpressionHelper.GetExpressionText(expression);
            var sb = new StringBuilder();

            if (listOfValues != null)
            {
                // Create a radio button for each item in the list 
                foreach (SelectListItem item in listOfValues)
                {
                    // Generate an id to be given to the radio button field 
                    var id = string.Format("rb_{0}_{1}",
                      fullName.Replace("[", "").Replace(
                      "]", "").Replace(".", "_"), item.Value);

                    // Create and populate a radio button using the existing html helpers 
                    var label = htmlHelper.Label(id, item.Text);
                    //var radio = htmlHelper.RadioButtonFor(expression, item.Value, new { id = id }).ToHtmlString();
                    var radio = htmlHelper.RadioButton(fullName, item.Value, item.Selected, new { id = id }).ToHtmlString();

                    // Create the html string that will be returned to the client 
                    // e.g. <input data-val="true" data-val-required=
                    //   "You must select an option" id="TestRadio_1" 
                    //    name="TestRadio" type="radio"
                    //   value="1" /><label for="TestRadio_1">Line1</label> 
                    sb.AppendFormat("<div class=\"RadioButton\">{0}{1}</div>",
                       radio, label);
                }
            }

            return MvcHtmlString.Create(sb.ToString());
        }

        public static MvcHtmlString EnumDropDownListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression, IEnumerable<SelectListItem> listOfValues)
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            IEnumerable<TEnum> values = Enum.GetValues(typeof(TEnum)).Cast<TEnum>();

            IEnumerable<SelectListItem> items =
                listOfValues.Select(value => new SelectListItem
                {
                    Text = value.Text,
                    Value = value.Value,
                    Selected = value.Selected
                });

            return htmlHelper.DropDownListFor(
                expression,
                items
                );
        }
    }
}