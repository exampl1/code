﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace C2.Logging
{
    public class NLogger : BaseLogger
    {
        public NLogger(Logger logger)
        {
            _logger = logger;
        }

        public NLogger(string name)
            : this(LogManager.GetLogger(name))
        {
        }

        public override void Log(LogMessageSeverity severity,
            string messageFormat,
            params object[] formatParams)
        {
            LogLevel level = LogLevel.Off;
            switch (severity)
            {
                case LogMessageSeverity.Debug:
                    level = LogLevel.Debug;
                    break;
                case LogMessageSeverity.Error:
                    level = LogLevel.Error;
                    break;
                case LogMessageSeverity.Fatal:
                    level = LogLevel.Fatal;
                    break;
                case LogMessageSeverity.Information:
                    level = LogLevel.Info;
                    break;
                case LogMessageSeverity.Trace:
                    level = LogLevel.Trace;
                    break;
                case LogMessageSeverity.Warning:
                    level = LogLevel.Warn;
                    break;
            }
            
            _logger.Log(level, messageFormat, formatParams);
        }

        Logger _logger;
    }
}
