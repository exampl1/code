﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DAL.SqlClient.Builder
{
    public abstract class TableValueFunction : IDataSource, ISchemableEntity
    {
        protected TableValueFunction(string name, string schema = null, IEnumerable<ISqlExpression> parameters = null)
        {
            Name = name;
            Schema = schema;
        }

        public string Name
        {
            get;
            set;
        }

        public string Schema
        {
            get;
            set;
        }

        public string Alias
        {
            get;
            set;
        }

        IEnumerable<ISqlExpression> _parameters;

        public void ToFromSql(TextWriter writer)
        {
            writer.Write(this.GetFullName());
            if (_parameters != null)
            {
                bool isFirst = true;
                foreach (var p in _parameters)
                {
                    if (!isFirst)
                    {
                        writer.Write(", ");
                    }
                    else
                    {
                        isFirst = false;
                    }
                    p.ToSql(writer);
                }
            }
            writer.Write(")");
            if (!string.IsNullOrWhiteSpace(Alias))
            {
                writer.Write(" AS [{0}]", Alias);
            }
        }

        public string Reference
        {
            get
            {
                return string.IsNullOrEmpty(Alias)
                    ? this.GetFullName()
                    : string.Format("[{0}]", Alias);
            }
        }
    }
}
