﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DAL.SqlClient.Builder
{
    public abstract class View : IDataSource, ISchemableEntity
    {
        protected View(string name, string schema = null)
        {
            Name = name;
            Schema = schema;
        }

        public string Name
        {
            get;
            set;
        }

        public string Schema
        {
            get;
            set;
        }

        public string Alias
        {
            get;
            set;
        }

        public void ToFromSql(TextWriter writer)
        {
            writer.Write(this.GetFullName());
            if (!string.IsNullOrWhiteSpace(Alias))
            {
                writer.Write(" AS [{0}]", Alias);
            }
        }

        public string Reference
        {
            get
            {
                return string.IsNullOrEmpty(Alias)
                    ? this.GetFullName()
                    : string.Format("[{0}]", Alias);
            }
        }
    }
}
