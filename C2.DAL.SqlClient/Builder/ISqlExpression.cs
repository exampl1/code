﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DAL.SqlClient.Builder
{
    public interface ISqlExpression
    {
        void ToSql(TextWriter writer);
    }

    public static class SqlExpressionHelper
    {
        public static void ToSql(this IEnumerable<ISqlExpression> items, TextWriter writer)
        {
            foreach (var item in items)
            {
                item.ToSql(writer);
            }
        }

        public static string ToSql(this ISqlExpression expression)
        {
            StringWriter w = new StringWriter();
            expression.ToSql(w);
            return w.ToString();
        }
    }
}
