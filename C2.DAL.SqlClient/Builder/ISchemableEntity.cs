﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DAL.SqlClient.Builder
{
    public interface ISchemableEntity
    {
        string Name
        {
            get;
        }

        string Schema
        {
            get;
        }
    }

    public static class SchemaBindedEntityHelper
    {
        public static string GetFullName(this ISchemableEntity entity)
        {
            return entity.Schema == null ?
                string.Format("[{0}]", entity.Name) :
                string.Format("[{0}].[{1}]", entity.Schema, entity.Name);
        }
    }
}
