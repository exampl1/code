﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DAL.SqlClient.Builder
{
    public class Query : ISqlExpression
    {
        List<QueryItem> _items = new List<QueryItem>();

        public Query(QuerySpecification query)
        {
            _items.Add(new QueryItem
            {
                QuerySpecification = query
            });
        }

        public Query Union(QuerySpecification query)
        {
            _items.Add(new QueryItem
            {
                QuerySpecification = query,
                Operation = "UNION"
            });
            return this;
        }

        public Query UnionAll(QuerySpecification query)
        {
            _items.Add(new QueryItem
            {
                QuerySpecification = query,
                Operation = "UNION ALL"
            });
            return this;
        }

        public Query Except(QuerySpecification query)
        {
            _items.Add(new QueryItem
            {
                QuerySpecification = query,
                Operation = "EXCEPT"
            });
            return this;
        }

        public Query Intersect(QuerySpecification query)
        {
            _items.Add(new QueryItem
            {
                QuerySpecification = query,
                Operation = "INTERSECT"
            });
            return this;
        }

        class QueryItem
        {
            public QuerySpecification QuerySpecification;

            public string Operation;
        }

        public void ToSql(TextWriter writer)
        {
            if (_items.Count == 0)
            {
                return;
            }

            for (var i = 0; i < _items.Count; i++)
            {
                var item = _items[i];
                if (i > 0)
                {
                    writer.WriteLine(" " + item.Operation);
                }
                item.QuerySpecification.ToSql(writer);
            }
        }
    }
}
