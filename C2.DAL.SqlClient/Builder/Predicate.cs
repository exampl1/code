﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DAL.SqlClient.Builder
{
    public class Predicate : ISearchConditionItem
    {
        protected Predicate(IEnumerable<ISqlExpression> items)
        {
            _items = items;
        }

        protected Predicate(params ISqlExpression[] items)
        {
            _items = items;
        }

        IEnumerable<ISqlExpression> _items;

        public void ToSql(TextWriter writer)
        {
            _items.ToSql(writer);
        }

        public static Predicate Compare(ISqlExpression lvalue, ISqlExpression rvalue, ComparisonOperator op)
        {
            return new Predicate(lvalue, op.ToSql(), rvalue);
        }

        static Predicate Like(IStringExpression match, IStringExpression pattern, bool not, char? escape)
        {
            List<ISqlExpression> items = new List<ISqlExpression>
            {
                match,
                (not ? " NOT LIKE " : " LIKE ").ToSql(),
                pattern
            };
            if (escape.HasValue)
            {
                items.Add(" ESCAPE '".ToSql());
                items.Add(escape.Value.ToSql());
                items.Add("'".ToSql());
            }

            return new Predicate(items);
        }

        public static Predicate Like(IStringExpression match, IStringExpression pattern, char? escape = null)
        {
            return Like(match, pattern, false, escape);
        }

        public static Predicate NotLike(IStringExpression match, IStringExpression pattern, char? escape = null)
        {
            return Like(match, pattern, true, escape);
        }

        public static Predicate Between(ISqlExpression value, ISqlExpression lbound, ISqlExpression rbound)
        {
            return new Predicate(value, " BETWEEN ".ToSql(), lbound, " AND ".ToSql(), rbound);
        }

        public static Predicate NotBetween(ISqlExpression value, ISqlExpression lbound, ISqlExpression rbound)
        {
            return new Predicate(value, " NOT BETWEEN ".ToSql(), lbound, " AND ".ToSql(), rbound);
        }

        public static Predicate IsNull(ISqlExpression value)
        {
            return new Predicate(value, " IS NULL ".ToSql());
        }

        public static Predicate IsNotNull(ISqlExpression value)
        {
            return new Predicate(value, " IS NOT NULL ".ToSql());
        }

        public static Predicate FreeText(IStringExpression freeText, IEnumerable<string> columns = null)
        {
            return new Predicate(" FREETEXT ((".ToSql(),
                columns == null ? "*".ToSql() : string.Join(", ", columns).ToSql(),
                "), ".ToSql(),
                freeText,
                ")".ToSql());
        }

        public static Predicate In(ISqlExpression value, Query subquery)
        {
            return new Predicate(value, " IN (".ToSql(), subquery, ")".ToSql());
        }

        public static Predicate NotIn(ISqlExpression value, Query subquery)
        {
            return new Predicate(value, " NOT IN (".ToSql(), subquery, ")".ToSql());
        }

        static Predicate In(ISqlExpression value, IEnumerable<ISqlExpression> options, bool negative)
        {
            List<ISqlExpression> items = new List<ISqlExpression> { value, (negative ? " NOT IN(" :  " IN (").ToSql() };
            bool isFirst = true;
            foreach (var option in options)
            {
                if (!isFirst)
                {
                    items.Add(", ".ToSql());
                }
                items.Add(option);
                isFirst = false;
            }
            items.Add(")".ToSql());

            return new Predicate(items);
        }

        public static Predicate In(ISqlExpression value, IEnumerable<ISqlExpression> options)
        {
            return In(value, options, false);
        }

        public static Predicate NotIn(ISqlExpression value, IEnumerable<ISqlExpression> options)
        {
            return In(value, options, true);
        }

        public static Predicate All(ISqlExpression value, Query subquery, ComparisonOperator op)
        {
            return new Predicate(value, op.ToSql(), "ALL (".ToSql(), subquery, ")".ToSql());
        }

        public static Predicate Any(ISqlExpression value, Query subquery, ComparisonOperator op)
        {
            return new Predicate(value, op.ToSql(), "ANY (".ToSql(), subquery, ")".ToSql());
        }

        public static Predicate Exists(Query subquery)
        {
            return new Predicate(" EXISTS (".ToSql(), subquery, ")".ToSql());
        }
    }

    public enum ComparisonOperator
    {
        Equal,

        NotEqual,

        GreatThan,

        GreatOrEqual,

        NotGreatThan,

        LessThan,

        LessOrEqual,

        NotLessThan
    }

    public static class ComparisonOperatorHelper
    {
        public static SqlLiteral ToSql(this ComparisonOperator value)
        {
            switch (value)
            {
                case ComparisonOperator.Equal:
                    return " = ";
                case ComparisonOperator.NotEqual:
                    return " <> ";
                case ComparisonOperator.GreatThan:
                    return " > ";
                case ComparisonOperator.GreatOrEqual:
                    return " >= ";
                case ComparisonOperator.NotGreatThan:
                    return " !> ";
                case ComparisonOperator.LessThan:
                    return " < ";
                case ComparisonOperator.LessOrEqual:
                    return " <= ";
                case ComparisonOperator.NotLessThan:
                    return " !< ";
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
