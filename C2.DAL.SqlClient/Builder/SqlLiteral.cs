﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DAL.SqlClient.Builder
{
    public struct SqlLiteral : ISqlExpression
    {
        public SqlLiteral(string value)
            : this()
        {
            Value = value;
        }

        public string Value
        {
            get;
            private set;
        }

        public void ToSql(TextWriter writer)
        {
            if (!string.IsNullOrWhiteSpace(Value))
            {
                writer.Write(Value);
            }
        }

        public static implicit operator SqlLiteral(string value)
        {
            return new SqlLiteral(value);
        }
    }

    public static class SqlLiteralHelper
    {
        public static SqlLiteral ToSql(this char value)
        {
            return new SqlLiteral(new string(value, 1));
        }

        public static SqlLiteral ToSql(this string value)
        {
            return new SqlLiteral(value);
        }
    }
}
