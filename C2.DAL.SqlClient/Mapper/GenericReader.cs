﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace C2.DAL.SqlClient.Mapper
{
    public class GenericReader<TEntity>
    {
        List<IMemberReader<TEntity>> _memberReaders = new List<IMemberReader<TEntity>>();

        public void Read(SqlDataReader reader, ref int index, TEntity entity)
        {
            foreach (var pr in _memberReaders)
            {
                pr.Read(reader, index, entity);
                index++;
            }
        }
    }
    
    public interface IMemberReader<TEntity>
    {
        void Read(SqlDataReader reader, int index, TEntity entity);
    }

    public class GenericMemberReader<TEntity, TMember> : IMemberReader<TEntity>
    {
        public GenericMemberReader(Expression<Func<TEntity, TMember>> accessor)
        {
            BuildSetter(accessor);
        }

        Action<TEntity, TMember> _setter;

        public void Read(SqlDataReader reader, int index, TEntity entity)
        {
            TMember value;
            if (reader.IsDBNull(index))
            {
                value = default(TMember);
            }
            else
            {
                value = reader.GetFieldValue<TMember>(index);
            }
            _setter(entity, value);
        }

        void BuildSetter(Expression<Func<TEntity, TMember>> accessor)
        {
            var entityParameter = Expression.Parameter(typeof(TEntity));
            var valueParameter = Expression.Parameter(typeof(TMember));
            var body = Expression.Assign(accessor, Expression.Parameter(typeof(TMember)));
            _setter = Expression.Lambda<Action<TEntity, TMember>>(body,
                entityParameter, valueParameter).Compile();
        }
    }
}
