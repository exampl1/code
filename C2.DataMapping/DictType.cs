﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataMapping
{
    internal static class DictionaryType
    {
        static int CodeBase
        {
            get
            {
                return Properties.Settings.Default.DictionaryCodeBase;
            }
        }

        public static int ClassifierType
        {
            get
            {
                return CodeBase + 0;
            }
        }

        public static int ContactType
        {
            get
            {
                return CodeBase + 1;
            }
        }

        public static int LinkType
        {
            get
            {
                return CodeBase + 2;
            }
        }

        public static int SubjectActivity
        {
            get
            {
                return CodeBase + 3;
            }
        }

        public static int OrganizationOwnershipType
        {
            get
            {
                return CodeBase + 4;
            }
        }

        public static int AddressType
        {
            get
            {
                return CodeBase + 5;
            }
        }

        public static int PropertyType
        {
            get
            {
                return CodeBase + 6;
            }
        }
    }
}
