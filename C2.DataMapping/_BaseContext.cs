﻿using C2.DataModel;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2.DataModel.MediaModel;
using System.ComponentModel.DataAnnotations.Schema;
using C2.DAL;

namespace C2.DataMapping
{
    /// <summary>
    /// Бзовая модель.
    /// </summary>
    public abstract class BaseContext : DbContext
    {
        protected BaseContext()
        {
        }

        protected BaseContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
        }

        protected BaseContext(DbCompiledModel model)
            : base(model)
        {
        }

        protected BaseContext(string nameOrConnectionString, DbCompiledModel model)
            : base(nameOrConnectionString, model)
        {
        }

        protected BaseContext(DbConnection existingConnection, bool contextOwnsConnection)
            : base(existingConnection, contextOwnsConnection)
        {
        }

        protected BaseContext(ObjectContext objectContext, bool dbContextOwnsObjectContext)
            : base(objectContext, dbContextOwnsObjectContext)
        {
        }

        protected BaseContext(DbConnection existingConnection, DbCompiledModel model, bool contextOwnsConnection)
            : base(existingConnection, model, contextOwnsConnection)
        {
        }

        static IdGenerator _idGenerator;

        public static void InitIdGenerator(IdGenerator generator)
        {
            _idGenerator = generator;
        }

        public static TEntity CreateEntity<TEntity>()
            where TEntity : IBaseDataEntity, new()
        {
            return new TEntity()
            {
                Id = GetId()
            };
        }

        public static long GetId()
        {
            return _idGenerator.GetId();
        }

        public DbSet<Address> Addresses
        {
            get;
            set;
        }

        public DbSet<AddressType> AddressTypes
        {
            get;
            set;
        }

        /// <summary>
        /// Классификаторы.
        /// </summary>
        public DbSet<Classifier> Classifiers
        {
            get;
            set;
        }

        /// <summary>
        /// Локализация классфикаторов.
        /// </summary>
        public DbSet<ClassifierLocalization> ClassifierLocalizations
        {
            get;
            set;
        }

        /// <summary>
        /// Узлы классификаторов.
        /// </summary>
        public DbSet<ClassifierNode> ClassifierNodes
        {
            get;
            set;
        }

        ///// <summary>
        ///// Справочник типов классификаторов.
        ///// </summary>
        //public DbSet<ClassifierType> ClassifierTypes
        //{
        //    get;
        //    set;
        //}

        /// <summary>
        /// Комментарии.
        /// </summary>
        public DbSet<Comment> Comments
        {
            get;
            set;
        }

        /// <summary>
        /// Очередь почтовых сообщений.
        /// </summary>
        public DbSet<MailQueue> MailQueue
        {
            get;
            set;
        }

        /// <summary>
        /// Контент веб страниц
        /// </summary>
        public DbSet<PageContent> PageContents
        {
            get;
            set;
        }

        /// <summary>
        /// Контакты.
        /// </summary>
        public DbSet<Contact> Contacts
        {
            get;
            set;
        }

        /// <summary>
        /// Локализация контактов.
        /// </summary>
        public DbSet<ContactLocalization> ContactLocalizations
        {
            get;
            set;
        }

        /// <summary>
        /// Справочник типов контактов.
        /// </summary>
        public DbSet<ContactType> ContactTypes
        {
            get;
            set;
        }

        /// <summary>
        /// Справочник типов свойств.
        /// </summary>
        public DbSet<PropertyType> PropertyTypes
        {
            get;
            set;
        }

        /// <summary>
        /// Значения свойств сущностей.
        /// </summary>
        public DbSet<PropertyValue> PropertyValues
        {
            get;
            set;
        }

        /// <summary>
        /// Локализация справочников.
        /// </summary>
        public DbSet<DictionaryItemLocalization> DictionaryItemLocalizations
        {
            get;
            set;
        }

        /// <summary>
        /// Субъекты.
        /// </summary>
        public DbSet<Subject> Subjects
        {
            get;
            set;
        }

        /// <summary>
        /// Локализация субъектов.
        /// </summary>
        public DbSet<SubjectLocalization> SubjectLocalizations
        {
            get;
            set;
        }

        /// <summary>
        /// Справочник областей деятельности субъектов.
        /// </summary>
        public DbSet<SubjectActivityType> SubjectActivityTypes
        {
            get;
            set;
        }

        /// <summary>
        /// Элементы медиа-коллекций.
        /// </summary>
        public DbSet<MediaCollectionEntry> MediaCollectionEntries
        {
            get;
            set;
        }

        /// <summary>
        /// Медиа-элементы.
        /// </summary>
        public DbSet<MediaItem> MediaItems
        {
            get;
            set;
        }

        /// <summary>
        /// Медиа-элементы.
        /// </summary>
        public DbSet<Supplier> Suppliers
        {
            get;
            set;
        }

        /// <summary>
        /// Локализация медиа-элементов.
        /// </summary>
        public DbSet<MediaItemLocalization> MediaItemLocalizations
        {
            get;
            set;
        }

        /// <summary>
        /// Подпищики
        /// </summary>
        public DbSet<Subscriber> Subscribers
        {
            get;
            set;
        }
        
        public DbSet<SidEntity> Sids
        {
            get;
            set;
        }

        public DbSet<Job> Jobs
        {
            get;
            set;
        }


        public DbSet<UserProfile> UserProfiles
        {
            get;
            set;
        }

        public DbSet<Affiliate> Affiliates
        {
            get;
            set;
        }

        public DbSet<MembershipRecord> MembershipRecords
        {
            get;
            set;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Configurations
                .Add(new AddressMap())
                .Add(new AffiliateMap())
                .Add(new ClassifierLocalizationMap())
                .Add(new ClassifierMap())
                .Add(new ClassifierNodeMap())
                .Add(new CommentMap())
                .Add(new ContactLocalizationMap())
                .Add(new ContactMap())
                .Add(new DictionaryItemLocalizationMap())
                .Add(new MailQueueMap())
                .Add(new OrganizationLocalizationMap())
                .Add(new OrganizationMap())
                .Add(new PersonLocalizationMap())
                .Add(new PersonMap())
                .Add(new SubjectLocalizationMap())
                .Add(new SubjectMap())
                .Add(new PropertyValueMap())
                .Add(new MediaItemMap())
                .Add(new MediaItemLocalizationMap())
                .Add(new MediaCollectionEntryMap())
                .Add(new PageContentMap())
                .Add(new SubscriberMap())
                .Add(new SidMap())
                .Add(new JobMap())
                .Add(new MembershipRecordMap())
                .Add(new UserProfileMap())
                .Add(new SupplierMap());

            modelBuilder.Entity<DictionaryItem>()
                .Map<AddressType>(t => t.Requires(DictionaryItemMap.Discriminator).HasValue(DictionaryType.AddressType))
                //.Map<ClassifierType>(t => t.Requires(DictionaryItemMap.Discriminator).HasValue(DictionaryType.ClassifierType))
                .Map<ContactType>(t => t.Requires(DictionaryItemMap.Discriminator).HasValue(DictionaryType.ContactType))
                .Map<LinkType>(t => t.Requires(DictionaryItemMap.Discriminator).HasValue(DictionaryType.LinkType))
                .Map<OrganizationOwnershipType>(t => t.Requires(DictionaryItemMap.Discriminator).HasValue(DictionaryType.OrganizationOwnershipType))
                .Map<SubjectActivityType>(t => t.Requires(DictionaryItemMap.Discriminator).HasValue(DictionaryType.SubjectActivity))
                .Map<PropertyType>(t => t.Requires(DictionaryItemMap.Discriminator).HasValue(DictionaryType.PropertyType))
                .ToTable("DictionaryItem");

            modelBuilder.Entity<DictionaryItem>()
                .Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
        }
    }
}
