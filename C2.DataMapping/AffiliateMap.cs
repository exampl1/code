﻿using C2.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.DataMapping
{
    public class AffiliateMap : EntityTypeConfiguration<Affiliate>
    {
        public AffiliateMap()
        {
            ToTable("Affiliate");

            HasKey(t => t.UserId);

            Property(p => p.UserId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            HasMany(t => t.AffiliateUsers)
                .WithOptional()
                .HasForeignKey(d => d.ParentUserId);

            

                //.Map(t =>
                //    {
                //        t.ToTable("User");
                //        t.MapLeftKey("UserId");
                //        t.MapRightKey("ParentUserId");
                //    }
                //);
        }
    }
}
