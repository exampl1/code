﻿using C2.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;

namespace C2.DataMapping
{
    public class ContactLocalizationMap : EntityTypeConfiguration<ContactLocalization>
    {
        public ContactLocalizationMap()
        {
            HasKey(t => new { t.ContactId, t.CultureCode });
        }
    }
}
