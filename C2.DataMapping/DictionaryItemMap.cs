﻿using C2.DataModel;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.ComponentModel.DataAnnotations.Schema;

namespace C2.DataMapping
{
    public sealed class DictionaryItemMap : EntityTypeConfiguration<DictionaryItem>
    {
        public DictionaryItemMap()
        {
            ToTable("DictionaryItem");

            Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
        }

        public const string Discriminator = "DictType";
    }
}
