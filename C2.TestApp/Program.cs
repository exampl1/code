﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Order order = new Order
            {
                Number = "123",
                Total = 456.67m,
                Items = new List<OrderItem> { new OrderItem { Name = "A" }, new OrderItem { Name = "B" } }
            };

            var p = new OrderProcessor(order);
            Console.WriteLine(p.Execute());
        }
    }

    class OrderProcessor : SimpleTemplateProcessor
    {
        const string Tpl = @"<p>{{Number}}</p>" +
                "<p>Итого: {{Total}} р.</p>" +
                "<ol>" +
                "{{foreach Items Items.tpl}}" +
                "</ol>";

        public OrderProcessor(Order model)
            : base(Tpl)
        {
            _model = model;
        }

        Order _model;

        protected override string GetValue(string name)
        {
            switch (name)
            {
                case "Number":
                    return _model.Number;
                case "Total":
                    return _model.Total.ToString("N2");

            }

            throw new ArgumentException();
        }

        protected override IEnumerable GetEnumeration(string name)
        {
            switch (name)
            {
                case "Items":
                    return _model.Items;
            }

            throw new ArgumentException();
        }

        protected override SimpleTemplateProcessor CreateSubProcessor(string name, object model)
        {
            switch (name)
            {
                case "Items.tpl":
                    return new OrderItemProcessor(model as OrderItem);
            }

            throw new ArgumentException();
        }
    }

    class OrderItemProcessor : SimpleTemplateProcessor
    {
        const string Tpl = @"<li>{{Name}}</li>";

        public OrderItemProcessor(OrderItem model)
            : base(Tpl)
        {
            _model = model;
        }

        OrderItem _model;

        protected override string GetValue(string name)
        {
            switch (name)
            {
                case "Name":
                    return _model.Name;
            }

            throw new ArgumentException();
        }

        protected override IEnumerable GetEnumeration(string name)
        {
            throw new NotImplementedException();
        }

        protected override SimpleTemplateProcessor CreateSubProcessor(string name, object model)
        {
            throw new NotImplementedException();
        }
    }

    class Order
    {
        public string Number { get; set; }

        public decimal Total { get; set; }

        public List<OrderItem> Items { get; set; }
    }

    class OrderItem
    {
        public string Name { get; set; }
    }
}
