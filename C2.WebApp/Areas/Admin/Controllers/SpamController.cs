﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using C2.DataModel;
using C2.DataMapping;

namespace C2.WebApp.Areas.Admin.Controllers
{
    public class SpamController : Controller
    {
        private AppContext db = new AppContext("DefaultConnection");

        //
        // GET: /Admin/Spam/
        public ActionResult Index()
        {
            var model = new Models.SpamViewModel
            {
                Content = db.PageContents.Where(p => p.ContentType == "text/html-email"),
                Sids = db.Sids,
                Jobs = db.Jobs
            };

            if (Request.IsAjaxRequest())
            {
                //C2.WebApp.Filters.CompressFilter.CompressResponse(Request, Response);
                // compressed by IIS itself
                return PartialView("_List", model);
            }

            return View();
        }


        // GET: /Admin/Content/
        public ActionResult CreateEdit(long? id)
        {
            PageContent page = null;

            if (id.HasValue)
            {
                page = db.PageContents.Find(id.Value);
            }
            else
            {
                page = new PageContent { ContentType = "text/html-email" };
            }

            return View("~/Areas/Admin/Views/Content/CreateEdit.cshtml", page);
        }

        [HttpPost]
        public ActionResult ChangeJobState(string templateName, bool start, string time)
        {
            var job = db.Jobs.Where(j => j.TemplateName == templateName).SingleOrDefault();
            var result = "";

            DateTime startDate = DateTime.UtcNow;
            switch (time)
            {
                case "tomorrowMorning":
                    startDate = startDate.Date.AddDays(1).AddHours(7);
                    break;
                default:
                    break;
            }

            if (job != null)
            {
                if (start)
                {
                    job.Status |= EntityStatus.Enabled;
                    job.StartDate = startDate;
                    result = "Job start signal sent";
                }
                else
                {
                    job.Status &= ~EntityStatus.Enabled;
                    result = "Job stop signal sent";
                }

            }
            else if (start)
            {
                db.Jobs.Add(new Job
                {
                    Id = C2.DataManager.AppDataManager.GetId(),
                    Status = EntityStatus.Enabled,
                    TemplateName = templateName,
                    StartDate = startDate,
                });

                result = "Job start signal sent. New job";
            }

            db.SaveChanges();

            return new ContentResult { Content = result };
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}