﻿using C2.AppModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace C2.WebApp.Helpers
{
    public class JsonFormatter
    {
        static public void WriteList<T>(TextWriter writer, IEnumerable<T> list, Func<TextWriter, T, bool> writeItemFunc)
        {
            writer.Write("[");
            bool isfirst = true;
            foreach (var item in list)
            {
                if (!isfirst)
                {
                    writer.Write(",");
                }
                writeItemFunc(writer, item);
                isfirst = false;
            }
            writer.Write("]");
        }

        static public bool WriteNodeList(TextWriter writer, List<TreeNodeModel> nodes)
        {
            WriteList<TreeNodeModel>(writer, nodes, WriteTreeNode);
            return true;
        }


        static public bool WriteTreeNode(TextWriter writer, TreeNodeModel node)
        {
            writer.Write("{");
            WriteAttribute(writer, "data", node.Caption);

            writer.Write(", \"attr\": {");
            WriteAttribute(writer, "id", node.NodeId);
            writer.Write(",");
            WriteAttribute(writer, "path", node.Path);

            writer.Write("}");

            if (node.Nodes.Count > 0)
            {
                writer.Write(",");
                WriteAttribute(writer, "state", "closed");
            }

            writer.Write("}");
            return true;
        }



        static void WriteAttribute(TextWriter writer, string name, object value)
        {
            WriteAttribute(writer, name, value.ToString());
        }

        static void WriteAttribute(TextWriter writer, string name, long value)
        {
            writer.Write("\"{0}\":{1}", name, value);
        }

        static void WriteAttribute(TextWriter writer, string name, int value)
        {
            writer.Write("\"{0}\":{1}", name, value);
        }

        static void WriteAttribute(TextWriter writer, string name, string value)
        {
            writer.Write("\"{0}\":\"{1}\"", name, HttpUtility.JavaScriptStringEncode(value));
        }

        static void WriteAttribute(TextWriter writer, string name, bool value)
        {
            writer.Write("\"{0}\":{1}", name, value);
        }

    }
}