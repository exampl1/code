﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace C2.WebApp.Filters
{
    [ValidateInput(false)]
    public class PartnerAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!filterContext.RequestContext.HttpContext.User.Identity.IsAuthenticated)
            {
                if (C2.AppController.SupplierModel.Current.PartnerCode.HasValue)
                {
                    filterContext.RequestContext.HttpContext.Response.SetCookie(new System.Web.HttpCookie("partner")
                    {
                        Value = C2.AppController.SupplierModel.Current.PartnerCode.Value.ToString("#"),
                        Domain = filterContext.RequestContext.HttpContext.Request.Url.Host.Replace("www.", "").Insert(0, "."),
                        Expires = DateTime.Now.AddMonths(1),
                    });
                } else
                if (filterContext.HttpContext.Request.Params["p"] != null && filterContext.HttpContext.Request.Params["p"].ToString() != "undef")
                {
                    if (!filterContext.RequestContext.HttpContext.Request.Cookies.AllKeys.Contains("partner"))
                    {
                        string partnerValue = filterContext.HttpContext.Request.Params["p"].ToString();
                        filterContext.RequestContext.HttpContext.Response.SetCookie(new System.Web.HttpCookie("partner")
                        {
                            Value = partnerValue,
                            Domain = filterContext.RequestContext.HttpContext.Request.Url.Host.Replace("www.", "").Insert(0, "."),
                            Expires = DateTime.Now.AddMonths(1),
                        });
                    }
                }
            }

            base.OnActionExecuting(filterContext);
        }
    }
}