﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace C2.PackageVersionUpdater
{
    class Program
    {
        static void Main(string[] args)
        {
            var packages = LoadPackages(Environment.CurrentDirectory);
            
            // По умолчанию - увеличиваем ревизию.
            int partIndex = 2;
            string incArg = args.FirstOrDefault(a => a.StartsWith("-inc=", StringComparison.CurrentCultureIgnoreCase));
            if (!string.IsNullOrWhiteSpace(incArg))
            {
                incArg = incArg.ToLower();
                switch (incArg)
                {
                    case "-inc=version":
                    case "-inc=v":
                        partIndex = 0;
                        break;
                    case "-inc=subversion":
                    case "-inc=sv":
                        partIndex = 1;
                        break;
                    case "-inc=revision":
                    case "-inc=r":
                        partIndex = 2;
                        break;
                }
            }

            foreach (var package in packages)
            {
                package.IncrementVersion(partIndex);
            }

            foreach (var package in packages)
            {
                foreach (var subPackage in packages)
                {
                    subPackage.UpdateDependency(package);
                }
            }

            foreach (var package in packages)
            {
                package.Save();
            }
        }

        static List<Package> LoadPackages(string path)
        {
            List<Package> packages = new List<Package>();
            
            foreach (string fileName in Directory.EnumerateFiles(path, "*.nuspec"))
            {
                string filePath = Path.Combine(path, fileName);
                packages.Add(new Package(filePath));
            }
            
            return packages;
        }
    }

    class Package
    {
        public Package(string filePath)
        {
            FilePath = filePath;
            _document = new XmlDocument();
            _document.Load(filePath);
            
            OriginalVersion = CurrentVersion;
        }

        XmlDocument _document;

        public string Id
        {
            get
            {
                return _document.GetElementsByTagName("id")[0].FirstChild.Value;
            }
        }

        public string FilePath
        {
            get;
            private set;
        }

        public string OriginalVersion
        {
            get;
            private set;
        }

        public string CurrentVersion
        {
            get
            {
                return _document.GetElementsByTagName("version")[0].FirstChild.Value;
            }
            set
            {
                _document.GetElementsByTagName("version")[0].FirstChild.Value = value;
            }
        }

        public void IncrementVersion(int partIndex)
        {
            string rawVersion = CurrentVersion;
            string suffix = string.Empty;
            int hyphenIndex = rawVersion.IndexOf('-');
            if (hyphenIndex > -1)
            {
                suffix = rawVersion.Substring(hyphenIndex);
                rawVersion = rawVersion.Substring(0, hyphenIndex);
            }

            string[] items = rawVersion.Split('.');
            if (items.Length <= partIndex)
            {
                partIndex = items.Length - 1;
            }

            string rawPart = items[partIndex];
            int value = int.Parse(rawPart);
            items[partIndex] = (value + 1).ToString();

            for (int i = partIndex + 1; i < items.Length; i++)
            {
                items[i] = "0";
            }

            string newVersion = string.Join(".", items) + suffix;
            CurrentVersion = newVersion;
        }

        public void UpdateDependency(Package parentPackage)
        {
            var ds = _document.GetElementsByTagName("dependency");
            for (int i = 0; i < ds.Count; i++)
            {
                var node = ds.Item(i);
                if (node.Attributes["id"].Value == parentPackage.Id)
                {
                    node.Attributes["version"].Value = parentPackage.CurrentVersion;
                }
            }
        }

        public void Save()
        {
            _document.Save(FilePath);
        }
    }
}
