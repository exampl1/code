﻿using System;
namespace C2.DAL
{
    public interface ISelectResult
    {
        bool IsFirstPage { get; }
        bool IsLastPage { get; }
        int PageCount { get; }
        int PageIndex { get; }
        int PageSize { get; }
        long TotalCount { get; }
    }
}
